var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');
var request = require('request');
var fs = require('fs');

/*
  Initialize express app
 */
var app = express();
app.use(favicon(__dirname + '/dist/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());

var Firebase = require('firebase');
var FirebaseTokenGenerator = require('firebase-token-generator');
var tokenGenerator = new FirebaseTokenGenerator('AIHc0o2Fvn0BWAOLkKMTd0LWfO8I408d9wL2cLMA');
var token = tokenGenerator.createToken({uid: "fanzee_server"});

require.extensions['.html'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

var jwtCheck = jwt({
  secret: new Buffer('Z3-fHJ12tSHCwR9GJi-Oglj6pm_x4Hg9nqRBP0TQR8Ioew9Tbt2mLLXTxU6-X-uM', 'base64'),
  audience: 'ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x'
});

/**
 * Development Settings
 */
if (app.get('env') === 'development') {
  // This will change in production since we'll be using the dist folder
  // This covers serving up the index page
  app.use(express.static(path.join(__dirname, '/dist')));

  // Error Handling
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
  message: err.message,
  error: err
});
  });
}

/**
 * Production Settings
 */
if (app.get('env') === 'production') {

  app.use(express.static(path.join(__dirname, '/dist')));

  // production error handler
  // no stacktraces leaked to user

  app.use(function(req, res) {
    res.sendfile(__dirname + '/dist/index.html');
});
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
  message: err.message,
  error: err
});
  });
}

module.exports = app;
