'use strict';

/**
 * @ngdoc overview
 * @name fanzeeShareApp
 * @description
 * # fanzeeShareApp
 *
 * Main module of the application.
 */
 angular
 .module('fanzeeShareApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ui.router', 
  'ui.router.state.events',
  'ngMaterial',
  'auth0',
  'angular-storage',
  'angular-jwt',
  'ngFileUpload',
  'cloudinary',
  'firebase',
  'base64',
  'luegg.directives',
  'ngMessages',
  'angular-spinkit',
  'mailchimp',
  'ngclipboard',
  'ngToast',
  'angularMoment',
  'uiCropper',
  'yaru22.angular-timeago',
  'angularModalService'
  ])
 .config(["$urlRouterProvider", "$locationProvider", "authProvider", "$httpProvider", "ngToastProvider", "jwtInterceptorProvider", "$stateProvider", "cloudinaryProvider", function ($urlRouterProvider, $locationProvider, authProvider, $httpProvider, ngToastProvider, jwtInterceptorProvider, $stateProvider, cloudinaryProvider) {



  $locationProvider.html5Mode(true);

  $stateProvider

  .state('main', {
    url:'/',
    templateUrl: 'views/main.html',
    controller: 'MainCtrl'
  })

  .state('landing', {
    url:'/landing/:brandId',
    templateUrl: 'views/landing.html',
    controller: 'LandingCtrl'
  })

  .state('share', {
    url:'/share/:brandId/:sourceType/:sourceData',
    templateUrl: 'views/share.html',
    controller: 'ShareCtrl',
    data: {
      requiresLogin: true
    },
    cache: false
  })

  .state('feed', {
    url:'/feed',
    templateUrl: 'views/feed.html',
    controller: 'FeedCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('rewards', {
    url:'/rewards',
    templateUrl: 'views/rewards.html',
    controller: 'RewardsCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('brand-detail', {
    url:'/rewards/brand/:brandId',
    templateUrl: 'views/brand-detail.html',
    controller: 'RewardsCtrl',
    data: {
      requiresLogin: true
    },
    params: {
        brandId: null
    }
  })

  .state('settings', {
    url:'/settings',
    templateUrl: 'views/settings.html',
    controller: 'SettingsCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('login', {
    url: '/login/:brandId',
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl',
    params: { brandId: null}
  });

  $urlRouterProvider.otherwise('/');

  authProvider.init({
    domain: AUTH0_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    loginState: 'login'
  });
  
  jwtInterceptorProvider.tokenGetter = ['store', function(store) {
    // Return the saved token
    return store.get('token');
  }];

  $httpProvider.interceptors.push('jwtInterceptor');
  
  ngToastProvider.configure({
    animation: 'slide',
    verticalPosition: 'top',
    horizontalPosition: 'center' 
  });

  cloudinaryProvider
  .set("cloud_name", "fanzee")
  .set("upload_preset", "hcc3xwxk");


}])

 .run(["$rootScope", "auth", "$stateParams", "store", "$state", "$timeout", "jwtHelper", "$location", function($rootScope, auth, $stateParams, store, $state, $timeout, jwtHelper, $location) {

  function clean(obj) {
    for (var propName in obj) { 
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }

  // This events gets triggered on refresh or URL change
  auth.hookEvents();
  //This event gets triggered on URL change
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
    if (!auth.isAuthenticated) {
      var token = store.get('token');
      var refreshToken = store.get('refreshToken');
      //store original url for redirect if not logged in
      if(!$location.path().includes('login') && $location.path() != '/'){
        $rootScope.redirectData = {
          brandId: toParams.brandId,
          sourceData: toParams.sourceData,
          sourceType: toParams.sourceType,
          redirectURL: $location.path()
        }
        clean($rootScope.redirectData);
      }
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          auth.authenticate(store.get('profile'), token).then(function(prof){
            if($rootScope.redirectData && $rootScope.redirectData.redirectURL){
              $location.path($rootScope.redirectData.redirectURL);
            }
          });
        } else {
          if (refreshToken) {
            return auth.refreshIdToken(refreshToken).then(function(idToken) {
              store.set('token', idToken);
              auth.authenticate(store.get('profile'), idToken).then(function(prof){
            if($rootScope.redirectData.redirectURL){
              $location.path($rootScope.redirectData.redirectURL);
            }
          });
            });
          } else {
            auth.signout();
            store.remove('token');
            store.remove('profile');
            store.remove('refreshToken');
            store.remove('firebaseToken');
            $state.go('login');
          }                          
        }
      }
    }
    else {
      if(auth.isAuthenticated && !auth.profile.identities[0].isSocial){
        if(!$location.path().includes('/login') && $location.path() != '/'){
          $rootScope.redirectData = {
            brandId: toParams.brandId,
            sourceData: toParams.sourceData,
            sourceType: toParams.sourceType,
            redirectURL: $location.path()
          }
          clean($rootScope.redirectData);
          console.log($rootScope.redirectData);
        }
        auth.signout();
        store.remove('token');
        store.remove('profile');
        store.remove('refreshToken');
        store.remove('firebaseToken');
        $state.go('login');
      };
    };
  });


  //broadcast state changes for spinners
  $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams, options){ 
      $rootScope.stateLoading = true;
    });
  $rootScope.$on('$stateChangeSuccess', 
    function(event, toState, toParams, fromState, fromParams){ 
      $timeout(function(){
        $rootScope.stateLoading = false;
      });

    });
}])

 .directive('resize', ["$window", function ($window) {
  return function (scope, element) {
    var w = angular.element($window);
    var changeHeight = function() {element.css('height', (w.height()) + 'px' );};  
    w.bind('resize', function () {        
              changeHeight();   // when window size gets changed             
            });  
        changeHeight(); // when page loads          
      };
    }])

 .directive('center', ["$window", "$timeout", function ($window, $timeout) {
  return function (scope, element, attrs) {
    var win = angular.element($window);
    var changeCss = function() {
    var adjustment = attrs.heightAdjust || 0;
    var elemHeight = element[0].offsetHeight;
      if(win.height() > elemHeight + adjustment){
        element.css('position', 'absolute' );
        element.css('top', '50%');
        element.css('left', '50%');
        element.css('transform', 'translate(-50%, -50%)');
      }
      else {
        element.css('position', 'relative' );
        element.css('top', '0');
        element.css('left', '0');
        element.css('transform', 'none');
      }
    };  
    win.bind('resize', function () {       
              changeCss();   // when window size gets changed             
            }); 
            $timeout(function() {
    changeCss(); // when page loads
})           
      };
    }])

 .directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
})

//compile dynamic templates
.directive('dynamic', ["$compile", function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
}])

.filter("as", ["$parse", function($parse) {
  return function(value, context, path) {
    return $parse(path).assign(context, value);
  };
}])

.filter('numKeys', function() {
  return function(object) {
    if(object){
      return Object.keys(object).length;
    }
    else {
      return 0;
    }
  }
})

.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});

var AUTH0_CLIENT_ID='ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x';
var AUTH0_CALLBACK_URL=location.href;
var AUTH0_DOMAIN='fanzee.auth0.com';

'use strict';

angular.module('fanzeeShareApp')

.service('Drafts', ["$firebaseArray", "$firebaseObject", "store", "Connections", "$state", "$base64", "auth", function($firebaseArray, $firebaseObject, store, Connections, $state, $base64, auth) {
  var baseRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login page
      }
    });

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login page
        $state.go('login');
      }
    });

  var userID = $base64.encode(auth.profile.user_id);

  var draftsRef = baseRef.child(userID).child("requests");

  var drafts = $firebaseArray(draftsRef);


  var userRef = baseRef.child(userID);

  var self = this;


  this.all = function() {
            //Reload drafts from Firebase to remove locally added feed items
            drafts = $firebaseArray(draftsRef);

            return drafts.$loaded().then(function(drafts) {
              //get all of the brands the user is connected with
              return Connections.connections().then(function(connections){
                angular.forEach(connections, function(value, key) {
                  if(value['brand_id']){
                  //load the feed for each brand connected with the user
                  var feed = $firebaseArray(brandsRef.child(value['brand_id']).child('feed'));
                  feed.$loaded()
                  .then(function(feed){
                    angular.forEach(feed, function(val, k) {

                      //get the draft data
                      var d = val;

                      self.hidden().then(function(hiddenRequests){

                        //Check if this request has been hidden(deleted) by the user
                        if(Object.values(hiddenRequests).indexOf(d.$id) <= -1){

                          //console.log(d);
                          //if it hasn't been hidden, push it to the local drafts array
                          drafts.push(d);

                        };

                      })
                      
                    });

                  });
                };
              });
                return drafts
              })
              .catch(function(err){
                alert("Could not connect to server");
              });


            })
            .catch(function(err){
              alert("Could not connect to server");
            });
          }

          this.get = function(source, brandID, draftID) {
           switch(source){
            case 'id':
            //In this case, we use the request_id child of the direct request to
            //get the data from the brands requests
            var requests = $firebaseArray(brandsRef.child(brandID).child('requests'));
            return requests.$loaded().then(function(data){
              return data.$getRecord(draftID).content;
            });
          }
        };

        this.save = function(draft) {
          drafts.$save(draft);
        };

        this.delete = function(draft) {
          if (draft.audience === 'feed'){
              //add to hidden so user doesn't see this feed item anymore
              userRef.child('hidden_requests').push(draft.$id);

            }
            //remove from user drafts object
            draftsRef.child(draft.$id).remove();
          };

          this.hidden = function() {

            var hiddenRequests = $firebaseObject(userRef.child('hidden_requests'));
            return hiddenRequests.$loaded().then(function(data){
              return data
            });
          };

        }])

.service('Twitter', ["$http", "Social", "auth", "Drafts", "$state", function($http, Social, auth, Drafts, $state) {

  this.share = function(draft, imgURI, imageData) {
    var brand_id = null;

    if(draft != null && draft.brand_id != null){
      brand_id = draft.brand_id;
    }; 

    if (imgURI != null) {
      var img = {
        'image': imageData,
        'user_id': auth.profile.user_id
      };
      //upload image if there is one selected by user
      return $http.post('https://fanzee.com/twitter/upload', img)
      .then(function(data) {

        var media_id = data.data.media_id_string;

        var data = {
          'status': draft.text,
          'user_id': auth.profile.user_id,
          'short_url': draft.short_url || null,
          'long_url': draft.long_url || null,
          'request_id': draft.request_id,
          'brand_id': brand_id,
          'media_ids': media_id
        };

      //post to postmedia if there is an image uploaded
      $http.post('https://fanzee.com/twitter/postmedia', data)
      .then(function(data) {
        return data;
        })   // success
      .catch(function(err) {
        console.log(err);    
      }); 
      })   // success
      .catch(function(err) {
        console.log(err);
      }); 
    } else {
      var data = {
        'status': draft.text,
        'user_id': auth.profile.user_id,
        'short_url': draft.short_url || null,
        'long_url': draft.long_url || null,
        'request_id': draft.request_id,
        'brand_id': brand_id
      };
      //post to post endpoint if there is no image uploaded
      return $http.post('https://fanzee.com/twitter/post', data)
      .then(function(data) {
        return data;
      })
      .catch(function(err) {
        console.log(err);
      }); 
    }
  };
}])

.service('Emails', ["$http", "$firebaseObject", "auth", "$state", "store", "$base64", function ($http, $firebaseObject, auth, $state, store, $base64) {

  var self = this;
  var profile = auth.profile;
  var influencerID = $base64.encode(profile.user_id);
  var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  this.connection = function(brandID, name) {
    var data = {
      'brand_id': brandID,
      'name': name    
    }
    $http.post('https://fanzee.com/mail/connection', data)
    .then(function() {
    })
    .catch(function(err) {
    })

  };


  this.message = function(brandID, name) {
    var data = {
      'brand_id': brandID,
      'name': name    
    }
    $http.post('https://fanzee.com/mail/message', data)
    .then(function() {
    })
    .catch(function(err) {
    })

  };

  this.userEmails = function() {
    var emails = $firebaseObject(influencersRef.child(influencerID).child('identity').child('emails'));

    return emails.$loaded()
    .then(function(result){
      return result;
    });
  };

  this.userSettings = function() {
    var settings = $firebaseObject(influencersRef.child(influencerID).child('identity').child('user_settings'));

    return settings.$loaded()
    .then(function(result){
      return result;
    });
  };



}])

.service('Connections', ["$firebaseArray", "$firebaseObject", "OpenData", "$http", "auth", "Emails", "store", "$state", "$base64", function($firebaseArray, $firebaseObject, OpenData, $http, auth, Emails, store, $state, $base64) {

  var profile = auth.profile;
  var influencerID = $base64.encode(profile.user_id);

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
  brandsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var identity = $firebaseObject(influencersRef.child(influencerID).child("identity"));

  this.allRequests = function() {

   var connection_requests = $firebaseArray(influencersRef.child(influencerID).child('connection_requests'));
   return connection_requests;

 };

 this.connections = function() {

  var connections = $firebaseArray(influencersRef.child(influencerID).child('connected'));

  return connections.$loaded()
  .then(function(result){
    return result;
  });

};

this.brandConnections = function (brand_id) {

  return connections= $firebaseArray(brandsRef.child(brand_id).child('influencers').child('connected')).$loaded(function(data){
    return data;
  });


};

this.add = function(brandId) {

  return OpenData.brandInfo(brandId)
  .then(function(brand){
      //add connection info for brand & influencer 
      influencersRef.child(influencerID).child('connected').push({'brand_name': brand.name, 'brand_id': brandId, 'brand_photo': brand.logo});
      brandsRef.child(brandID).child('influencers').child('connected').push({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': identity.picture});
      return true
    },
    function(err){
      console.log(err);
    });
}

this.approveRequest = function(request) {

  var brandID = request.brand_id;

      //add connection info for brand & influencer 
      influencersRef.child(influencerID).child('connected').push({'brand_name': request.brand_name, 'brand_id': request.brand_id, 'brand_photo': request.brand_photo});
      brandsRef.child(brandID).child('influencers').child('connected').push({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': identity.picture});

      //remove from requests for brand & influencer
      influencersRef.child(influencerID).child('connection_requests').child(request.$id).remove();
      brandsRef.child(brandID).child('influencers').child('requested').child(request.request_key).remove();

      //send email notification
      Emails.connection(request.brand_id, profile.name);
    };
  }])

.service('Social', ["auth", "$http", "$state", "store", function (auth, $http, $state, store) {

 var prof = auth.profile;     

 this.profiles = {
  'facebook':{},
  'twitter':{},
  'instagram':{},
  'linkedin':{}
};
    var self = this; //passes current "this" to http.get

    this.isTwitter = function() {
      for (var i = 0; i < prof.identities.length; i++) {
        if (angular.equals(prof.identities[i].provider, "twitter")) {
         return true;
       }
     }
     return false;
   };

   this.isInstagram = function() {
    for (var i = 0; i < prof.identities.length; i++) {
      if (angular.equals(prof.identities[i].provider, "instagram")) {
       return true;
     }
   }
   return false;
 };

 this.isFacebook = function() {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "facebook")) {
     return true;
   }
 }
 return false;
}; 

this.isLinkedIn = function() {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "linkedin")) {
     return true;
   }
 }
 return false;
};

auth.getProfile().then(function(profile) {
  self.rebuildIdentities(profile);
});

this.rebuildIdentities = function (prof) {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "facebook")) {
      var data = {
        'user_id': prof.user_id     
      };
      $http.post('https://fanzee.com/facebook/verify', data)
      .then(function(data) {

        self.profiles.facebook.followers = data.data.friends;
        self.profiles.facebook.name = data.data.name;
        self.profiles.facebook.photo = data.data.photo;
        if(data.data.name == undefined) {
          self.tokenRefresh('facebook');
        };
      })
      .catch(function(err) {
        //handle error
      })
      $http.post('https://fanzee.com/facebook/pages-list', data)
      .then(function(data) {

        if(data.data.data) {
          self.profiles.facebook.pages = data.data.data;
        }

        if(data.data.error) {
          self.tokenRefresh('facebook');
        };
      })
      .catch(function(err) {
        //handle error
        console.log(err);
      })

    } 
    if (angular.equals(prof.identities[i].provider, "twitter")) {
     var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/twitter/verify', data)
    .then(function(data) {

      self.profiles.twitter.followers = data.data.followers_count;
      self.profiles.twitter.name = "@" + data.data.screen_name;
      self.profiles.twitter.photo = data.data.profile_image_url_https;
      if(data.data.screen_name == undefined) {
        self.tokenRefresh('twitter');
      };
    })
    .catch(function(err) {
      //handle error
    }) 
  }
  else if (angular.equals(prof.identities[i].provider, "instagram")) {

    var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/instagram/verify', data)
    .then(function(data) {

      self.profiles.instagram.followers =  data.data.counts.followed_by;
      self.profiles.instagram.name = "@" + data.data.username;
      self.profiles.instagram.photo = data.data.profile_picture;
      if(data.data.username == undefined) {
        self.tokenRefresh('instagram');
      };
    })
    .catch(function(err) {
      //handle error
    })
  }
  else if (angular.equals(prof.identities[i].provider, "linkedin")) {

    var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/linkedin/verify', data)
    .then(function(data) {
      if(data.data.numConnectionsCapped == true){
        self.profiles.linkedin.followers = '500+';
      } else {
        self.profiles.linkedin.followers = data.data.numConnections;
      }
      self.profiles.linkedin.name =  data.data.firstName + ' ' + data.data.lastName;
      self.profiles.linkedin.photo = data.data.pictureUrl;
      if(data.data.numConnectionsCapped == undefined) {
        self.tokenRefresh('linkedin');
      };
    })
    .catch(function(err) {
      console.log(err);
      //handle error
    })
  }
};
};

this.tokenRefresh = function(platform) {
  var lock = new Auth0Lock('ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x', 'fanzee.auth0.com');
  lock.show({
    closable: true,
    icon: 'https://fanzee.com/images/white-menu-logo.ea2497ed.png',
    dict: {
      signin: {
        title: 'Please reverify your ' + platform + ' account'
      }
    },
    connections: [platform],
    authParams: {
     connection_scopes: {
      'linkedin': ['r_basicprofile', 'w_share']
    }
  }
}, function(err, profile, id_token){
  if(err){ 
    alert('Something went wrong.  Please try again!');
  }
  else {
    store.set('profile', profile);
    prof = profile;
    self.rebuildIdentities(profile);
  }
})
}

this.linkNewAccount = function () {

  var platforms = ['twitter', 'instagram', 'linkedin', 'facebook'];
    //remove already connected platforms
    if(self.isInstagram()) {
      var index = platforms.indexOf('instagram');
      if(index!=-1){
       platforms.splice(index, 1);
     }
   };
   if(self.isTwitter()) {
    var index = platforms.indexOf('twitter');
    if(index!=-1){
     platforms.splice(index, 1);
   }
 };
 if(self.isLinkedIn()) {
  var index = platforms.indexOf('linkedin');
  if(index!=-1){
   platforms.splice(index, 1);
 }
};
if(self.isFacebook()) {
  var index = platforms.indexOf('facebook');
  if(index!=-1){
   platforms.splice(index, 1);
 }
};

var lock = new Auth0Lock('ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x', 'fanzee.auth0.com');
lock.show({
  dict: {
    signin: {
      title: 'Link another account'
    }
  },
  icon: 'https://fanzee.com/images/white-menu-logo.ea2497ed.png',
  rememberLastLogin: false,
  connections: platforms,
  authParams: {
   connection_scopes: {
    'linkedin': ['r_basicprofile', 'w_share']
  }
}
}, function(err, profile, id_token){
  $http({
    method: "POST",
    headers: {'Authorization': 'Bearer '+ store.get('token') },
    url: "https://fanzee.auth0.com/api/v2/users/" + prof.user_id + "/identities",
    data: {
      'link_with': id_token
    }
  })
  .then(function(response) {

    auth.getProfile().then(function(profile){
      store.set('profile', profile);
      prof = profile;
      self.rebuildIdentities(profile);
      $state.go('main', {reload: true});
    });

  })
  .catch(function(err) {
    $state.go('login', {reload: true});
  })  
})
};
}])

.service('OpenData', ["$http", "auth", "$state", "$base64", "$firebaseArray", "store", function ($http, auth, $state, $base64, $firebaseArray, store) {

  var self = this;

  this.brandInfo = function (brand_id) {
    var url = 'https://fanzee.com/open/brand-info/' + brand_id;
    return $http.get(url)
    .then(function(response){
      var brand = response.data;
      var linkData = {
        name: response.data.name,
        id: brand_id,
        photo: response.data.logo,
        widget_messages: response.data.widget_messages
      }
      self.getDeeplink(linkData).then(function(result){
       brand.mobileLink = result;
     })
      return brand
    });
  }

  //Alternative connection function so we don't have to include the Connections service before user is logged in
  this.addConnection = function(brandId) {

    var profile = auth.profile;
    var influencerID = $base64.encode(profile.user_id);

    var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
    brandsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
      if (error) {
        $state.go('login');
      }
    });

    var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
    influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
      if (error) {
        $state.go('login');
      }
    });

    var connections = $firebaseArray(influencersRef.child(influencerID).child('connected'));

    return connections.$loaded()
    .then(function(result){
      for (var i = 0; i < result.length; i++){
       if (result[i].brand_id === brandId){
        return
      }
    }
    return self.brandInfo(brandId)
    .then(function(brand){
        //add connection info for brand & influencer 
        influencersRef.child(influencerID).child('connected').child('brand-' + brandId).set({'brand_name': brand.name, 'brand_id': brandId, 'brand_photo': brand.logo});
        brandsRef.child(brandId).child('influencers').child('connected').child('adv-' + influencerID).set({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': profile.picture});
        return
      },
      function(err){
        return err
        console.log(err);
      });
  });
  }

  this.getDeeplink = function (brand) {
    var linkData = {
      'brand': {
        name: brand.name,
        id: brand.id,
        photo: brand.photo
      }
    };
    return $http.post('https://fanzee.com/deeplink/invite', linkData)
    .then(function (data) {
      return data.data;
    })
    .catch(function (err) {
      console.log(err);
    });
  }

}])

.service('LinkedIn', ["auth", "$http", "$state", "store", "Social", "Drafts", function (auth, $http, $state, store, Social, Drafts) {
  this.share = function(draft) {
   var brand_id = draft.brand_id;

   if(draft.short_url)
   {
    var share_url = draft.short_url;
  } else {
    var share_url = draft.long_url;
  }

  var data = {
    'comment': draft.text || null,
    'user_id': auth.profile.user_id,
    'share_url': share_url,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url,
    'og_image': draft.og_image || null,
    'request_id': draft.request_id,
    'brand_id': brand_id
  };

  return $http.post('https://fanzee.com/linkedin/post', data)
  .then(function(data) {
    return data;
  })
  .catch(function(err) {
    console.log(err);
    return err;
  }); 
};

}])

.service('Facebook', ["auth", "$http", "$state", "store", "Social", "Drafts", function (auth, $http, $state, store, Social, Drafts) {

  this.shareText = function(draft) {

    var brand_id = draft.brand_id;

    var data = {
      'comment': draft.text,
      'user_id': auth.profile.user_id,
      'request_id': draft.request_id,
      'brand_id': brand_id,
      'short_url': draft.short_url || null,
      'long_url': draft.long_url || null
    };

    return $http.post('https://fanzee.com/facebook/post', data)
    .then(function(data) {
      return data;

    })
    .catch(function(err) {

      return err;
    }); 

  };

  this.shareImageURL = function(draft, imageURL) {

   var brand_id = draft.brand_id;

   var data = {
    'caption': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'image_uri': imageURL,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  };

  return $http.post('https://fanzee.com/facebook/image-upload-by-link', data)
  .then(function(data) {
    return data;

  })
  .catch(function(err) {
    alert(JSON.stringify(err));
    return err;
  }); 
};

this.shareImageURLPage = function(draft, page_id, imageURL) {

  var brand_id = draft.brand_id;

  var data = {
    'caption': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'image_uri': imageURL,
    'page_id': page_id,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  };

  return $http.post('https://fanzee.com/facebook/pages/image-upload-by-link', data)
  .then(function(data) {
    return data;

  })
  .catch(function(err) {
    alert(JSON.stringify(err));
    return err;
  }); 
};

this.shareTextPage = function(draft, page_id) {

  var brand_id = draft.brand_id;

  var data = {
    'comment': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'page_id': page_id,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  }; 

  return $http.post('https://fanzee.com/facebook/pages/post', data)
  .then(function(data) {
    return data;
  })
  .catch(function(err) {
    console.log(err);
    return err;
  }); 
};

}])

.service('LinkedIn', ["auth", "$http", "$state", "store", "Social", "Drafts", function (auth, $http, $state, store, Social, Drafts) {


  this.share = function(draft) {

    if(draft != null && draft.brand_id != null){
     var brand_id = draft.brand_id;

     if(draft.short_url)
     {
      var share_url = draft.short_url;
    } else {
      var share_url = draft.long_url;
    }

    var data = {
      'comment': draft.text || null,
      'user_id': auth.profile.user_id,
      'share_url': share_url,
      'short_url': draft.short_url || null,
      'long_url': draft.long_url,
      'og_image': draft.og_image || null,
      'request_id': draft.request_id,
      'brand_id': brand_id
    };

    return $http.post('https://fanzee.com/linkedin/post', data)
    .then(function(data) {
      return data;
    })
    .catch(function(err) {
      console.log(err);
      return err;
    }); 
  } 
};
}])

.service('ShortLinks', ["$http", "$base64", "auth", function ($http, $base64, auth) {
  var self = this;

  var userID = $base64.encode(auth.profile.user_id);

  this.shorten = function(url) {

    var data = {
      'url':  url + '?fzuser=' + userID
    }

  //shorten link so clicks can be tracked
  return $http.post('https://fanzee.com/google/shorten', data)
  .then(function(response) {
    return response.data;
  })
  .catch(function(err) {
    console.log(err);
  })

};

}])

.service('Leaderboards', ["$http", "$base64", "auth", "$firebaseArray", "$firebaseObject", "store", function ($http, $base64, auth, $firebaseArray, $firebaseObject, store) {

  var self = this;

  var leaderboardsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/leaderboards/");
  leaderboardsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login state
        $state.go('login');
      }
    });

  this.getLeaderboard = function(brandID, leaderboardID) {

    if(leaderboardID === 'all_time_leaderboard' || leaderboardID === 'monthly_leaderboard' || leaderboardID === 'weekly_leaderboard') {
      var board = $firebaseObject(leaderboardsRef.child(brandID).child(leaderboardID));
    }
    else {
      //Need function to get list of custom leaderboards and another to create a fb object for a particular one by $id
      var board = $firebaseObject(leaderboardsRef.child(brandID).child('custom').child(leaderboardID));
    }

    return board.$loaded(function(data) {

       return data; // true
     },
     function(error) {
      console.error("Error:", error);
    });

  };

  this.listCustom = function(brandID) {


    var customLeaderboards = $firebaseArray(leaderboardsRef.child(brandID).child('custom'));

    return customLeaderboards.$loaded(function(data){
      return data;
    });

  };

  this.getUserRankings = function (user_id) {
   return $http({
    url: 'https://fanzee.com/advocate/' + user_id + '/rankings-view' , 
    method: "GET"
  }).then(function(data){
    return data;
  }).catch(function(err) {
    console.log(err);
    return err;
  }); 
}

}])

.service('CloudinaryUpload', function () {

  this.uploadImage = function(imgURI) {

    var server = 'https://api.cloudinary.com/v1_1/fanzee/image/upload';

    var options = {
      'params': {
        'upload_preset': 'twlaajzw'
      }
    };

      /*return $cordovaFileTransfer.upload(server, imgURI, options)
      .then(function(result) {

        var response = JSON.parse(result.response);

        return response.secure_url;
        // Success!
      }, function(err) {
        console.log(JSON.stringify(err));
        return err;
        // Error
      });*/

      
    };
  })

.service('Messages', ["$firebaseArray", "$firebaseObject", "auth", "store", "$state", "Emails", "$base64", "Connections", function($firebaseArray, $firebaseObject, auth, store, $state, Emails, $base64, Connections) {

  var baseRef = new Firebase("https://shining-inferno-4546.firebaseio.com/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var brands = $firebaseArray(baseRef.child('brands'));

  var fb_id = $base64.encode(auth.profile.user_id);

  var messageRoomsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/message_rooms");
  var identity = $firebaseObject(baseRef.child("users").child("mobile").child(fb_id).child("identity"));

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands");

  var self = this;

  this.selectedRoom = null;
  var messageRooms = messageRoomsRef.orderByChild("influencer").equalTo(fb_id);

  this.messageRooms = function() {
    return $firebaseArray(messageRooms);
  };

  this.brands = function() {
    var connectedBrands = Connections.connections();
    return connectedBrands.$loaded()
    .then(function(result){
      return result;
    });
  };

  this.getBrand = function(brandID) {
    return brands.$loaded().then(function(data){
      return data.$getRecord(brandID);
    });
  };

  this.getRoom = function(roomID) {
    var rooms = self.messageRooms();
    return rooms.$loaded().then(function(data){
      return data.$getRecord(roomID);
    });
  };

  this.load = function (roomId) {
    var messages = $firebaseArray(messageRoomsRef.child(roomId).child('messages'));
    return messages.$loaded().then(function(messageArray){
      return messageArray
    });
  };

  this.setRoom = function(room) {
    self.selectedRoom = room;
  };

  this.createRoom = function(brandID) {
    var newRoom = {};
    newRoom.influencer = $base64.encode(auth.profile.user_id);
    newRoom.brand = brandID;
    var newRoomRef = messageRoomsRef.push(newRoom);
    var selected = newRoomRef.key();
    self.selectedRoom = self.getRoom(selected);
  };

  this.add = function(newMessage, userName, userImage) {

    messageRoomsRef.child(self.selectedRoom.$id).child('messages').push({ 'content': newMessage, 'from': userName, 'image': identity.picture });
    Emails.message(self.selectedRoom.brand, userName);
    self.alertBrand();

  };

  this.alertBrand = function() {;
    var brand = self.selectedRoom.brand
    var influencer = self.selectedRoom.influencer;

    var alert = $firebaseArray(brandsRef.child(brand).child("influencers").child("connected").orderByChild("influencer_id").equalTo(influencer)).$loaded().then(function(alert){

      var connection_id = alert[0].$id;
      brandsRef.child(brand).child("influencers").child("connected").child(connection_id).update({
        "new_message": "true"
      });


    });
  };

  this.clearAlert = function(connection) {

    baseRef.child("users").child("mobile").child(fb_id).child("connected").child(connection).update({
      "new_message": "false"
    });

  };
}])

.service('Rewards', ["$http", "$base64", "auth", "$firebaseArray", "$firebaseObject", "store", function ($http, $base64, auth, $firebaseArray, $firebaseObject, store) {

  var self = this;

  var rewardsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/rewards/");
  rewardsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login state
        $state.go('login');
      }
    });

  this.getBrandRewards = function(brand_id) {
    var rewards = $firebaseArray(rewardsRef.child(brand_id));
    return rewards.$loaded()
    .then(function(result){
      return result;
    });
  };
}]);

'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fanzeeShareApp
 */
angular.module('fanzeeShareApp')
  .controller('MainCtrl', ["$scope", "$state", "auth", "ModalService", function ($scope, $state, auth, ModalService) {

    $scope.loggedIn = false;

  	$scope.go = function(state){
  		$state.go(state);
  	}

    $scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

    auth.getProfile().then(function(data){
      $scope.loggedIn = true;
    },
    function(err){
      $scope.loggedIn = false;
      $state.go('login');
    });
   
  }])
  .controller('MenuCtrl', ["$timeout", "$scope", "$state", "$mdSidenav", function($timeout, $scope, $state, $mdSidenav) {
     $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    };

    $scope.goTo = function(state){
    	$state.go(state);
	};
}]);
  

'use strict';

/**
 * @ngdoc function
 * @name fanzeeWebApp.controller:LoginCtrl
 * @description
 * # Dashboard
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller( 'LoginCtrl', ["$scope", "auth", "$location", "OpenData", "$rootScope", "$stateParams", "$timeout", "$state", "store", "ngToast", function($scope, auth, $location, OpenData, $rootScope, $stateParams, $timeout, $state, store, ngToast) {

  $scope.loading = true;

  if(auth.isAuthenticated){
    console.log(auth.profile);
    $scope.loggedIn = true;
  }
  else {
    $scope.loggedIn = false;
  }

  /*Load brand data from various stateParams or rootScope variable, else no brand specified*/
  function loadBrand() {
    if($stateParams.brandId || $rootScope.redirectData && $rootScope.redirectData.brandId) {
      if ($stateParams.brandId){
        OpenData.brandInfo($stateParams.brandId)
        .then(function(info){
          $scope.loading = false;
          $scope.brand = info;
        },
        function(err){
          $scope.brand = null;
          $scope.loading = false;
          console.log(err);
        });
      }
      else{
        OpenData.brandInfo($rootScope.redirectData.brandId)
        .then(function(info){
          console.log('info ', info);
          $scope.loading = false;
          $scope.brand = info;
        },
        function(err){
          $scope.brand = null;
          $scope.loading = false;
          console.log(err);
        });
      }
    }
    else {
      $scope.brand = null;
      $scope.loading = false;
    }
  }

  $timeout(loadBrand());

  $scope.signin = function() {

    if($scope.brand){
      var logo = $scope.brand.logo;
    }
    else {
      var logo = 'https://fanzee.com/images/white-menu-logo.ea2497ed.png';
    }

    auth.signin({ disableSignupAction: false, connections: ['facebook', 'twitter', 'linkedin', 'instagram'], icon: logo, primaryColor: '#108FAD'},
     function(profile, token, accessToken, state, refreshToken) {
      store.set('profile', profile);
      store.set('token', token);
      store.set('refreshToken', refreshToken);
      auth.getToken({
        api: 'firebase'
      }).then(function(delegation) {
        store.set('firebaseToken', delegation.id_token);
        if($stateParams.brandId || $rootScope.redirectData && $rootScope.redirectData.brandId){
        var brandId = $stateParams.brandId || $rootScope.redirectData.brandId;
        $scope.loading = true;
        OpenData.addConnection(brandId).then(function(){
          $scope.loading = false;
          if($rootScope.redirectData && $rootScope.redirectData.redirectURL){
          var redirect = $rootScope.redirectData.redirectURL;
          $location.path(redirect);
        }
        $scope.loggedIn = true;
        });
      }
      else {
        $scope.loggedIn = true;
        $location.path('/');
      }
      }, function(error) {
        // Error getting the firebase token
        ngToast.danger({
          content: 'Please disable popup or ad blockers!'
        });
      })
    }, function() {
      // Error callback
      ngToast.danger({
        content: 'Please disable popup or ad blockers!'
      });
    });
  }
}])

 .controller( 'BrandLoginCtrl', ["$scope", "auth", "OpenData", "Connections", "$stateParams", "$state", "store", "ngToast", function($scope, auth, OpenData, Connections, $stateParams, $state, store, ngToast) {

  $scope.connectCheck = function(){
    var brandId = $stateParams.brandId;
    if(auth.isAuthenticated && brandId){
      OpenData.addConnection(brandId).then(function(){
        return
      },
      function(err){
        alert(err);
      })
    }
  }

}]);
'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the fanzeeShareApp
 */
angular.module('fanzeeShareApp')
  .controller('FeedCtrl', ["$scope", "$state", "ModalService", "Drafts", "auth", function ($scope, $state, ModalService, Drafts, auth) {

  	$scope.feedLoading = true;
	Drafts.all()
	  .then(function(drafts){
	   	$scope.drafts = drafts;
	   	$scope.feedLoading = false;
	  },
	  function(err){
	  	$scope.feedLoading = false;
	   	$state.go('login');
	  })

	  $scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj);
 		}
 		else{
 			$state.go(state);
 		}
 	}

 	$scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

  }]);
  
'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:ShareCtrl
 * @description
 * # ShareCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('ShareCtrl', ["$scope", "$state", "ModalService", "OpenData", "$mdDialog", "Twitter", "Facebook", "LinkedIn", "$stateParams", "$window", "$timeout", "ShortLinks", "Drafts", "auth", "Social", function ($scope, $state, ModalService, OpenData, $mdDialog, Twitter, Facebook, LinkedIn, $stateParams, $window, $timeout, ShortLinks, Drafts, auth, Social) {

   $scope.$watch(
    function(){ return auth.profile},
    function(newVal) {
      $scope.profile = newVal;
      $scope.buildProfile();
    }, true
    );

   $scope.$watch(
    function(){ return Social.profiles},
    function(newVal) {
      $scope.buildProfile();
    },
    true
    );

   $scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

   $scope.draft = {text: null};

   $scope.draftLoading = true;

   $scope.buildProfile = function (){
    $scope.instagram = Social.profiles.instagram;
    $scope.twitter = Social.profiles.twitter; 
    $scope.facebook = Social.profiles.facebook; 
    $scope.linkedin = Social.profiles.linkedin;
    $scope.isTwitter =  Social.isTwitter();
    $scope.isFacebook = Social.isFacebook();
    $scope.isInstagram = Social.isInstagram();
    $scope.isLinkedIn = Social.isLinkedIn();
  }

  $scope.isPlatform = function(platform) {
    switch(platform){
      case 'twitter':
        return $scope.isTwitter
        break
      case 'linkedin':
        return $scope.isLinkedIn
        break
      case 'facebook':
        return $scope.isFacebook
        break
      case 'instagram':
        return $scope.isInstagram
    }
  }

  $scope.buildProfile();

  $timeout(function(){
    OpenData.brandInfo($stateParams.brandId)
        .then(function(info){
          $scope.brand = info;
        },
        function(err){
          console.log(err);
        });

    Drafts.get($stateParams.sourceType, $stateParams.brandId, $stateParams.sourceData).then(function(draft){
      $scope.draft = draft;
      if($scope.draft.audience === 'feed' || $scope.draft.audience === 'link'){
        $scope.draft.request_id = draft.$id;
      };

      if ($scope.draft.link) {
        $scope.draft.long_url = $scope.draft.link;
        try {
          ShortLinks.shorten($scope.draft.link).then(function (data) {
            $scope.draft.short_url = data;
          });
        } catch (Exception) {
          console.log(Exception);
        }
      }
      $scope.draftLoading = false;
      reorderPlatforms();
    }, function(err){
      $scope.draftLoading = false;
    });
  });

  //Put synced platforms first in order 
  function reorderPlatforms() {
    if(!$scope.isInstagram && $scope.draft.platforms.indexOf('instagram') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('instagram'), 1)[0]);
    }
    if(!$scope.isFacebook && $scope.draft.platforms.indexOf('facebook') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('facebook'), 1)[0]);
    } 
    if(!$scope.isLinkedIn && $scope.draft.platforms.indexOf('linkedin') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('linkedin'), 1)[0]);
    }
    if(!$scope.isTwitter && $scope.draft.platforms.indexOf('twitter') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('twitter'), 1)[0]);
    }
    $scope.setPlatform($scope.draft.platforms[0]);
  }

  $scope.addPlatform = function(platform){
  //please link X platform alert
  Social.linkNewAccount();
  }

$scope.addToPost = function(content) {
    if($scope.draft.text == null || $scope.draft.text == '') {
      $scope.draft.text = content;
    } else {
      $scope.draft.text += " "+ content + " ";
    }
    //NgToast 'added to post!'
  }

$scope.share = function(platform, draft) {

   var alert = $mdDialog.alert()
            .title('Too many characters!')
            .content('Tweets must be 140 characters or less')
            .ariaLabel('Twitter warning')
            .ok('Ok');

  switch(platform){
    case 'twitter':
      if (draft.short_url){
        if(!draft.text){
          draft.text = null;
        }
        draft.text = draft.text + ' ' + draft.short_url;
      }
      if(draft.text.length > 140){
        $mdDialog.show(alert);
        return
      }
      Twitter.share(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      });
      break
    case 'facebook':
      Facebook.shareText(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      });
      break
    case 'linkedin':
      LinkedIn.share(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      }); 
  }
}

function handleShared() {
 var index = $scope.draft.platforms.indexOf($scope.selectedPlatform.name);
 $scope.draft.text = '';
   var alert = $mdDialog.alert()
            .title('Thanks for sharing!')
            .content('Keep sharing to earn more points and unlock rewards!  Simply close this window when you are finished.')
            .ariaLabel('Thanks for sharing')
            .ok('Ok');

      $mdDialog.show(alert).then(function () {
        if(index === $scope.draft.platforms.length - 1 &&  $scope.draft.platforms.length != 1){
            $scope.setPlatform(scope.draft.platforms[index - 1]);
            return
          }
          if($scope.draft.platforms.length != 1){
          $scope.setPlatform($scope.draft.platforms[index + 1]);
          return
        }
        return
        });
};

$scope.contentClick = function() {
  if($scope.draft.long_url){
   var confirm = $mdDialog.confirm()
            .title('What would you like to do?')
            .content('You can copy the preview text into your post or view this link in your browser')
            .ariaLabel('Open Link')
            .ok('Open Link')
            .cancel('Copy Text')
            .clickOutsideToClose(true);

      $mdDialog.show(confirm).then(function(res) {
        $window.open($scope.draft.long_url, '_blank');
    }, function() {
    $scope.addToPost($scope.draft.details);
  });
  }
  else {
    return
  }
}

$scope.setPlatform = function(platform) {
  $scope.selectedPlatform = {};
  $scope.selectedPlatform.name = platform;
  switch(platform){
    case 'twitter':
      $scope.selectedPlatform.data = $scope.twitter;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#00aced'};
    break
    case 'facebook':
      $scope.selectedPlatform.data = $scope.facebook;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#3B5998'};
    break
    case 'linkedin':
      $scope.selectedPlatform.data = $scope.linkedin;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#0077b5'};
    break
    case 'instagram':
      $scope.selectedPlatform.data = $scope.instagram;
      $scope.instagramSelected = true;
      $scope.selectedPlatform.color = {'background-color': '#bc2a8d'};
  }
}

  $scope.go = function(state, paramObj){
    if(paramObj){
      $state.go(state, paramObj)
    }
    else{
      $state.go(state);
    }
  }

}]);

'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('SettingsCtrl', ["$scope", "$state", "ModalService", "auth", "$base64", "Emails", "Social", function ($scope, $state, ModalService, auth, $base64, Emails, Social) {

 	$scope.$watch(
 		function(){ return auth.profile},
 		function(newVal) {
 			$scope.profile = newVal;
 			$scope.buildProfile();
 			$scope.profileLoading = true;
 			Emails.userEmails()
 			.then(function(emails){
 				$scope.userEmails = emails;
 				Emails.userSettings()
 				.then(function(settings){
 					$scope.userSettings = settings;
 					$scope.profileLoading = false;
 				},
 				function(error){
 					$scope.profileLoading = false;
 				})
 			}, function(err){
 				$scope.profileLoading = false;
 			})
 		}
 		);

 	$scope.$watch(
 		function(){ return Social.profiles},
 		function(newVal) {
 			$scope.buildProfile();
 		},
 		true
 		);

 	$scope.buildProfile = function (){
 		$scope.instagram = Social.profiles.instagram;
 		$scope.twitter = Social.profiles.twitter; 
 		$scope.facebook = Social.profiles.facebook; 
 		$scope.linkedin = Social.profiles.linkedin;
 		$scope.isTwitter =  Social.isTwitter();
 		$scope.isFacebook = Social.isFacebook();
 		$scope.isInstagram = Social.isInstagram();
 		$scope.isLinkedIn = Social.isLinkedIn();
 	}

 	$scope.buildProfile();

 	$scope.isPlatform = function(platform) {
 		switch(platform){
 			case 'twitter':
 			return $scope.isTwitter
 			break
 			case 'linkedin':
 			return $scope.isLinkedIn
 			break
 			case 'facebook':
 			return $scope.isFacebook
 			break
 			case 'instagram':
 			return $scope.isInstagram
 		}
 	}

 	$scope.updateSettings = function(object){
 		object.$save();
 	}

 	$scope.addEmail = function(address){
 		var addressKey = $base64.encode(address);
 		$scope.userEmails[addressKey] = {
 			address: address,
 			isActive: true
 		}
 		$scope.updateSettings($scope.userEmails);
 	}

 	$scope.addPlatform = function(platform){
 		Social.linkNewAccount();
 	}

 	$scope.openInfoModal = function() {
 		ModalService.showModal({
 			templateUrl: "views/info-modal.html",
 			controller: "MainCtrl"
 		}).then(function(modal) {
 			modal.element.modal();
 		});
 	}

 	$scope.openEmailModal = function() {
 		ModalService.showModal({
 			templateUrl: "views/add-email-modal.html",
 			controller: "SettingsCtrl",
 			scope: $scope
 		}).then(function(modal) {
 			modal.element.modal();
 		});
 	}

 	$scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj)
 		}
 		else{
 			$state.go(state);
 		}
 	}

 }]);

'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:RewardsCtrl
 * @description
 * # RewardsCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('RewardsCtrl', ["$scope", "auth", "$base64", "$filter", "ModalService", "$state", "Messages", "Connections", "Rewards", "$stateParams", "Leaderboards", function ($scope, auth, $base64, $filter, ModalService, $state, Messages, Connections, Rewards, $stateParams, Leaderboards) {

 	$scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj)
 		}
 		else{
 			$state.go(state);
 		}
 	}

  $scope.message = {text: null};
 	$scope.messageRooms = Messages.messageRooms();
 	$scope.brandsLoading = true;
 	$scope.showMessages = false;
 	$scope.user_id = $base64.encode(auth.profile.user_id);

 	Connections.connections()
 	.then(function(connections){
 		$scope.brands = connections;
 		$scope.brandsLoading = false;
 	},
 	function(err){
 		$scope.brandsLoading = false;
 	});

 	$scope.$watch(
 		function(){ return $stateParams},
 		function(newVal) {
 			if($stateParams.brandId){
 				$scope.brandLoading = true;
 				$scope.currentTime = new Date().getTime();
 				$scope.setRoom($stateParams.brandId);
 				Messages.getBrand($stateParams.brandId).then(function(data){
 					$scope.selectedBrand = data;
 					$scope.brandLoading = false;
 				},
 				function(err){
 					$state.go('rewards');
 				});
 				$scope.setRoom($stateParams.brandId);
 				loadRewards($stateParams.brandId);
 			};
 		}, true
 		);

 	$scope.setRoom = function(brandID) {
 		for(var i=0, len = $scope.messageRooms.length; i < len; i++){
 			if ($scope.messageRooms[i].brand == brandID) {
 				Messages.setRoom($scope.messageRooms[i]);
        Messages.load($scope.messageRooms[i].$id).then(function(array){
          $scope.messagesArray = array;
          return;
        }); 
        return
 			} 
 		}
 		Messages.createRoom(brandID);
 	}

  $scope.sendMessage = function() {
     if($scope.message.text){
      Messages.add($scope.message.text, auth.profile.name, auth.profile.picture);
      $scope.message.text = null;
    }
  }

  $scope.copiedAlert = function() {
    var alert = $mdDialog.alert()
            .title('Code copied')
            .content('The coupon code has been copied to your clipboard')
            .ariaLabel('Copied alert')
            .ok('Ok');
            $mdDialog.show(alert);
  }

 	function loadRewards(brandID){
 		$scope.rewardsLoading = true;
 		$scope.userRewards = {};
 		Rewards.getBrandRewards(brandID)
 		.then(function(rewards){
 		$scope.rewards = rewards;
          //Get relative rankings for each leaderboard
          Leaderboards.getUserRankings($scope.user_id)
          .then(function(rankings){
          	var rankings = rankings.data;
          	for (var i = 0; i < rewards.length; i++){
              //get ranking for leaderboard and append it to reward object for display
              if($scope.rewards[i].leaderboard_ref){
              	for(var z = 0; z < rankings.brands.length; z++){
              		if(rankings.brands[z].brand_id ===  brandID){
              			for (var j = 0; j < rankings.brands[z].leaderboards.custom_leaderboards.length; j++){
              				if (rankings.brands[z].leaderboards.custom_leaderboards[j].id === $scope.rewards[i].leaderboard_ref){
              					$scope.rewards[i].rank = rankings.brands[z].leaderboards.custom_leaderboards[j].rank;
              					$scope.rewards[i].current_points = rankings.brands[z].leaderboards.custom_leaderboards[j].points;
              				}
              			}
              		}
              	}
              }
              //create array of rewards user has already claimed 
              if($scope.rewards[i].claims){
              	for(var claim in $scope.rewards[i].claims){
              		if ($scope.rewards[i].claims.hasOwnProperty(claim)){
              			if($scope.rewards[i].claims[claim].advocate_id === $scope.user_id){
              				$scope.userRewards[$scope.rewards[i].$id] = $scope.rewards[i];
              				break
              			};
              		}
              	};
              }

          };
          	$scope.rewardsLoading = false;
            $scope.brandLoading = false;
      },
      function(error){
      	alert('Oops! Something went wrong.');
      	$state.go('rewards');
      });
      },function(error){
      	alert('Oops! Something went wrong.');
      	$state.go('rewards');
      });
 	};

  //In case user comes to nested state from push notification
  if($stateParams && $stateParams.brandId){
  	loadRewards($stateParams.brandId);
  };

  $scope.toggleMessages = function() {
  	$scope.showMessages = !$scope.showMessages;
  }

  //Brief description of how reward is unlocked
  $scope.rewardString = function(reward) {
  	var rewardString = null;
  	switch(reward.type){
  		case 'CONTEST':
  		rewardString = 'Rewarded by contest';
  		break;
  		case 'POST':
  		rewardString = 'Rewarded for first post';
  		break;
  		case 'SIGNUP':
  		rewardString = 'Rewarded at signup';
  		break;
  		case 'POINTS':
  		rewardString = 'Rewarded at ' + reward.points_thresh + ' points';
  		break;
  		default:
  		rewardString = null;
  	}
  	return rewardString;
  }

  $scope.openRewardModal = function(reward, isUnlocked) {
  	$scope.selectedReward = reward;
  	$scope.selectedReward.isUnlocked = isUnlocked;
  	$scope.setRequirementString(reward);
  	ModalService.showModal({
  		templateUrl: "views/reward-modal.html",
  		controller: "RewardsCtrl",
  		scope: $scope
  	}).then(function(modal) {
  		modal.element.modal();
  	});
  }

  $scope.openInfoModal = function() {
  	ModalService.showModal({
  		templateUrl: "views/info-modal.html",
  		controller: "MainCtrl"
  	}).then(function(modal) {
  		modal.element.modal();
  	});
  }



//Detailed string of how reward is unlocked
$scope.setRequirementString = function(reward) {
	switch(reward.type){
		case 'CONTEST':
		if(reward.leaderboard_ref){
			Leaderboards.getLeaderboard($scope.selectedBrand.$id, reward.leaderboard_ref)
			.then(function(leaderboard){
				var start_date = $filter('date')(leaderboard.start, "MM/dd/yyyy");
				var end_date = $filter('date')(leaderboard.end, "MM/dd/yyyy");
				$scope.requirementString = 'Have a top ' + reward.top_n + ' score from ' + start_date + ' to ' + end_date + '.';
			});
		}
		else{
			$scope.requirementString = 'Error retrieving requirements!';
		};
		break;
		case 'POST':
		$scope.requirementString = 'Share a post for this brand to unlock this reward.';
		break;
		case 'SIGNUP':
		$scope.requirementString = 'This reward unlocks when you first sign up.';
		break;
		case 'POINTS':
		$scope.requirementString = 'Rewarded at ' + reward.points_thresh + ' points.';
		break;
		default:
		$scope.requirementString = 'Error retrieving requirements!';
	}

}

}]);

angular.module('fanzeeShareApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/add-email-modal.html',
    "<div class=\"modal fade centered-modal\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" ng-click=\"close('Cancel')\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button> <strong> Add E-mail </strong> </div> <div class=\"modal-body\"> <form> <div class=\"form-group\"> <label for=\"e-mail-address\" class=\"form-control-label\">E-mail Address:</label> <input type=\"text\" class=\"form-control\" id=\"e-mail-address\" ng-model=\"emailAddress\"> </div> </form> </div> <div class=\"modal-footer\"> <button type=\"button\" ng-disabled=\"!emailAddress\" ng-click=\"addEmail(emailAddress)\" class=\"btn btn-primary\" data-dismiss=\"modal\">Add</button> </div> </div> </div></div>"
  );


  $templateCache.put('views/brand-detail.html',
    "<div class=\"view-wrapper\"> <wave-spinner ng-if=\"brandLoading || rewardsLoading\"></wave-spinner> <div ng-show=\"!brandLoading && !rewardsLoading\" layout=\"row\" layout-align=\"center center\"> <div class=\"card main-card\" center> <h1 class=\"text-center\"> <i class=\"fa fa-arrow-left pull-left\" ng-click=\"go('rewards')\"> </i> <img class=\"header-logo\" ng-src=\"{{selectedBrand.photo}}\"> <span ng-show=\"!showMessages\"> Rewards </span> <span ng-show=\"showMessages\"> Messages </span> </h1> <div class=\"row\"> <div class=\"col-md-6\"> <span class=\"message-link\" ng-click=\"toggleMessages()\" ng-class=\"{'active': !showMessages}\"> <i class=\"fa fa-gift\"> </i> View Rewards </span> </div> <div class=\"col-md-6\"> <span class=\"message-link\" ng-click=\"toggleMessages()\" ng-class=\"{'active': showMessages}\"> <i class=\"fa fa-comments\"> </i> View Messages </span> </div> </div> <p class=\"placeholder-note\" ng-hide=\"brands.length\"> You haven't joined any advocacy programs yet </p> <div class=\"scroll-box rewards-box\" ng-hide=\"showMessages\"> <div class=\"section-header\"> My Rewards </div> <md-list> <md-list-item ng-repeat=\"reward in userRewards\" class=\"md-2-line\" ng-click=\"openRewardModal(reward, true)\"> <img class=\"md-avatar\" ng-src=\"{{reward.image}}\" on-error-src=\"images/reward.png\" ng-if=\"reward.image\"> <img class=\"md-avatar\" ng-if=\"!reward.image\" src=\"images/reward.png\"> <div class=\"md-list-item-text\" layout=\"column\"> <p><strong>{{reward.name}}</strong></p> <p>{{rewardString(reward)}}</p> </div> </md-list-item> </md-list> <div class=\"rewards-placeholder\" ng-show=\"(userRewards | numKeys) < 1\"> You haven't earned any rewards yet. Share some content to earn points, win contests, and unlock rewards! </div> <div class=\"section-header\"> Available Rewards </div> <md-list> <md-list-item ng-repeat=\"reward in rewards\" class=\"md-2-line\" ng-show=\"reward.active\" ng-click=\"openRewardModal(reward, false)\"> <img class=\"md-avatar\" ng-src=\"{{reward.image}}\" on-error-src=\"images/reward.png\" ng-if=\"reward.image\"> <img class=\"md-avatar\" ng-if=\"!reward.image\" src=\"images/reward.png\"> <div class=\"md-list-item-text\" layout=\"column\"> <p><strong>{{reward.name}}</strong></p> <p>{{rewardString(reward)}}</p> </div> </md-list-item> </md-list> <div class=\"rewards-placeholder\" ng-show=\"!rewards.length\"> This company hasn't added any rewards yet. </div> </div> <div ng-show=\"showMessages\"> <div scroll-glue class=\"message-box scroll-box\"> <md-list scroll-glue> <md-list-item ng-repeat=\"message in messagesArray\" class=\"md-2-line\"><img ng-src=\"{{message.image}}\" err-src=\"images/empty_profile.jpg\" class=\"md-avatar\"> <div class=\"md-list-item-text\" layout=\"column\"> <h3><strong>{{ message.from }}</strong></h3> <p><strong>{{ message.content }}</strong></p> </div> </md-list-item> </md-list> </div> </div> <div ng-show=\"showMessages\"> <input class=\"message-input\" type=\"text\" placeholder=\"Type your message here\" ng-model=\"message.text\"> <button class=\"btn btn-message btn-blue pull-right\" ng-click=\"sendMessage()\"><strong> <i class=\"fa fa-arrow-right\"></i></strong></button> </div> <button type=\"button\" ng-click=\"openInfoModal()\" class=\"btn btn-primary info-button\" data-dismiss=\"modal\"><i class=\"fa fa-question\"></i></button> </div> </div> </div>"
  );


  $templateCache.put('views/feed.html',
    "<div class=\"view-wrapper\"> <wave-spinner ng-if=\"feedLoading\"></wave-spinner> <div ng-show=\"!feedLoading\" layout=\"row\" layout-align=\"center center\"> <div class=\"card main-card\" center> <h1> <i class=\"fa fa-home pull-left\" ng-click=\"go('main')\"> </i> My Feed </h1> <p class=\"placeholder-note\" ng-if=\"!drafts.length\"> There is no content here. Check back later. </p> <div ng-if=\"drafts.length\" class=\"scroll-box\"> <div ng-repeat=\"draft in drafts | orderBy : '-time'\" class=\"card feed-card\"> <div class=\"card-header\"> <md-list-item class=\"md-2-line\"> <img ng-src=\"{{draft.brand_logo}}\" err-src=\"images/empty_profile.jpg\" class=\"md-avatar\"> <div class=\"md-list-item-text\"> <p><strong>{{draft.brand_name}}</strong></p> <p>{{draft.time | timeAgo}}</p> </div> </md-list-item> </div> <div class=\"card-body\"> <div class=\"card-top\" ng-click=\"contentClick()\"> <h4 class=\"card-title\">{{draft.title}}</h4> <div class=\"image-wrapper\" ng-show=\"draft.image || draft.og_image\"> <img class=\"card-img-top\" ng-src=\"{{draft.image || draft.og_image}}\"> </div> <p class=\"card-text\">{{draft.details}}</p> <p class=\"card-link\" ng-show=\"draft.long_url\">{{draft.long_url}}</p> </div> <div class=\"social-icons\"> <span ng-repeat=\"platform in draft.platforms\"> <i class=\"fa fa-{{platform}}\"> </i> </span> </div> <div class=\"share-buttons\"> <button class=\"btn btn-primary btn-share\" ng-click=\"go('share', {brandId: draft.brand_id, sourceType: 'id', sourceData: draft.request_id || draft.$id})\" ng-style=\"selectedPlatform.color\"> <i class=\"fa fa-share\"></i>&nbsp;Share </button> </div> </div> </div> </div> <button type=\"button\" ng-click=\"openInfoModal()\" class=\"btn btn-primary info-button\" data-dismiss=\"modal\"><i class=\"fa fa-question\"></i></button> </div> </div> </div>"
  );


  $templateCache.put('views/info-modal.html',
    "<div class=\"modal fade centered-modal\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" ng-click=\"close('Cancel')\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button> <strong> FAQ </strong> </div> <div class=\"modal-body\"> <p><strong>What is all of this?</strong></p> <p>This is Fanzee social media advocacy.&nbsp; You were probably invited to join by a particular brand or community.&nbsp; Through this platform, you can earn rewards by sharing content to your social media accounts.&nbsp; Just share content that interests you to Facebook, Twitter, LinkedIn, or Instagram, and you're on your way to unlocking rewards!&nbsp;</p> <p><strong>Where do I find content to share?</strong></p> <p>Once invited to an advocacy program, you will find any content ready to share in your Feed, sent to your inbox, or sometimes the brand/community will distribute content by their own medium.&nbsp; To find out more about a specific program, navigate to the Rewards &amp; Messages section and send a message to whichever brand or community interests you.</p> <p><strong>How do I earn rewards?</strong></p> <p><span style=\"color: #333333\">Rewards are unlocked by sharing content to your personal social media accounts.&nbsp; Posting content to your followers, generating clicks on content, creating engagement, and more impact how many points you earn so share frequently and try to share quality content! Include hashtags or other elements requested by the brand to earn more points!</span></p> <p><strong><span style=\"color: #333333\">How do I find out what to do for a specific reward?</span></strong></p> <ul> <li><span style=\"color: #333333\">&nbsp;Navigate to the Rewards &amp; Messages area and select which brand or community you want to earn rewards from.</span></li> <li><span style=\"color: #333333\">Once in the rewards section, browse the available rewards and click one that interests you.&nbsp; The resulting dialog will describe the reward and how you can earn it!</span></li> </ul> <p><strong>Why are my points different for each reward?</strong></p> <p><span style=\"color: #333333\">The points you've earned toward each reward starts at 0 when the reward is created by the brand.&nbsp; So if you see a new reward you want to unlock, keep sharing!&nbsp; When you reach the points required to unlock a reward, that reward will automatically be unlocked for you.&nbsp; You can see your progress toward unlocking a reward by clicking it.</span></p> <p><strong><span style=\"color: #333333\">I've unlocked a reward, but haven't received it</span></strong></p> <p><span style=\"color: #333333\">If you have unlocked a reward, but haven't received it, simply navigate to the Rewards &amp; Messages section, click \"View Messages\" and send a message to the brand.&nbsp; Their advocacy team will respond to you as quickly as possible.&nbsp;&nbsp;</span></p> <p><strong><span style=\"color: #333333\">Why do I have to log in to share?</span></strong></p> <p><span style=\"color: #333333\">Logging in and sharing through this widget enables us to keep track of how effectively your post reaches other people.&nbsp; We will never post any content you don't specifically choose to post yourself.&nbsp;&nbsp;</span></p> </div> </div> </div> </div>"
  );


  $templateCache.put('views/login.html',
    "<div class=\"container-fluid view-padded\"> <wave-spinner ng-show=\"loading\"></wave-spinner> <div ng-if=\"!loading && loggedIn\" ng-controller=\"BrandLoginCtrl\" data-ng-init=\"connectCheck()\" layout=\"row\" layout-align=\"center center\"> <div center class=\"brand-data card\"> <img ng-src=\"{{brand.logo}}\"> <h3> <i class=\"fa fa-check-square-o\"> </i> <span> Thanks for signing up! </span> </h3> <span ng-show=\"!brand.widget_messages.welcome\"> <p> You're now part of the {{brand.name}} advocacy team! Keep an eye on your inbox for content to share. </p> </span> <span ng-show=\"brand.widget_messages.welcome\"> <p> {{brand.widget_messages.welcome}} </p> </span> </div> </div> <div ng-if=\"!loading && !loggedIn && brand\"> <div layout=\"row\" layout-align=\"center center\"> <div center class=\"brand-data card\"> <img ng-src=\"{{brand.logo}}\"> <span ng-show=\"!brand.widget_messages.signup\"> <h3> Welcome to {{brand.name}} advocacy! Share our content to earn rewards and prizes. </h3> </span> <span ng-show=\"brand.widget_messages.signup\"> <h3> {{brand.widget_messages.signup}} </h3> </span> <p> {{brand.name}} advocacy is powered by Fanzee, please sign up with a social provider. </p> <button ng-click=\"signin()\" class=\"btn btn-lg btn-primary\"> Okay, I'm ready! </button> <div class=\"app-link-wrapper\"> <a class=\"app-link\" ng-href=\"{{brand.mobileLink}}\"><i class=\"fa fa-mobile\"></i> Download the App</a> </div> </div> </div> </div> <div ng-if=\"!loading && !loggedIn && !brand\"> <div layout=\"row\" layout-align=\"center center\"> <div center class=\"brand-data card\"> <h3> Please login or sign up to view this page. </h3> <p> If you are attempting to join the advocacy team of a specific organization, please contact them for their unique link. If you are already a member, login below. </p> <button ng-click=\"signin()\" class=\"btn btn-lg btn-primary\" style=\"margin-bottom: 15px\"> Login </button> </div> </div> </div> </div>"
  );


  $templateCache.put('views/main.html',
    "<div class=\"container-fluid view-padded\"> <div center class=\"main-card card\"> <h1> My Advocacy Home </h1> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"main-button\" title=\"Find content to share\" ng-click=\"go('feed')\"> <i class=\"fa fa-newspaper-o\"></i> <div class=\"title\"> Feed </div> </div> </div> <div class=\"col-md-6\"> <div class=\"main-button\" title=\"Discover rewards to unlock & send/receive messages\" ng-click=\"go('rewards')\"> <i class=\"fa fa-gift\"></i> <div class=\"title\"> Rewards &amp; Messages </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"main-button\" title=\"Manage your account settings\" ng-click=\"go('settings')\"> <i class=\"fa fa-cogs\"></i> <div class=\"title\"> Settings </div> </div> </div> <div class=\"col-md-6\"> <div class=\"main-button\" title=\"Learn more\" ng-click=\"openInfoModal()\"> <i class=\"fa fa-question\"></i> <div class=\"title\"> FAQ </div> </div> </div> </div> </div></div>"
  );


  $templateCache.put('views/reward-detail.html',
    ""
  );


  $templateCache.put('views/reward-modal.html',
    "<div class=\"modal fade centered-modal\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" ng-click=\"close('Cancel')\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button> <md-list-item> <img class=\"md-avatar\" ng-src=\"{{selectedReward.image}}\" on-error-src=\"images/reward.png\" ng-if=\"selectedReward.image\"> <img class=\"md-avatar\" ng-if=\"!selectedReward.image\" src=\"images/reward.png\"> <p>{{selectedReward.name}}</p> </md-list-item> </div> <div class=\"modal-body\"> <md-list> <md-list-item class=\"md-2-line\" ng-show=\"!selectedReward.isUnlocked\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Reward Unlocked By</p> <p>{{requirementString}}</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.isUnlocked\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Reward Unlocked!</p> <p>{{selectedReward.claim_text}}</p> </div> </md-list-item> </md-list> <div class=\"row\"> <div class=\"col-md-6\"> <md-list> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.claim_link && selectedReward.isUnlocked\" ng-href=\"{{selectedReward.claim_link}}\" target=\"_blank\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Link</p> <p>Click here</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.isUnlocked && !selectedReward.claim_link\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Link</p> <p>N/A</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"!selectedReward.isUnlocked\" ng-click=\"\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Link</p> <p>Hidden</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.discount_code && selectedReward.isUnlocked\" ngclipboard data-clipboard-text=\"{{selectedReward.discount_code}}\" ng-click=\"copiedAlert()\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Code</p> <p>{{selectedReward.discount_code}}</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-click=\" \" ng-show=\"selectedReward.isUnlocked && !selectedReward.discount_code\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Code</p> <p>N/A</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"!selectedReward.isUnlocked\" ng-click=\"\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Code</p> <p>Hidden</p> </div> </md-list-item> </md-list> </div> <div class=\"col-md-6\"> <md-list> <md-list-item class=\"md-2-line\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Status</p> <p> <span ng-show=\"selectedReward.active && !selectedReward.isUnlocked\">Active</span> <span ng-show=\"!selectedReward.active && !selectedReward.isUnlocked\">Ended</span> <span ng-show=\"selectedReward.isUnlocked\">Unlocked</span> </p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.type === 'CONTEST'\" ng-click=\"showLeaderboard(selectedReward.leaderboard_ref)\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>My Rank</p> <p>{{numberSuffix(selectedReward.rank)}}</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.type === 'CONTEST'\" ng-click=\"showLeaderboard(selectedReward.leaderboard_ref)\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>Winners</p> <p>Top {{selectedReward.top_n | number}}</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.type === 'POINTS'\" ng-click=\"showLeaderboard(selectedReward.leaderboard_ref)\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>My Points</p> <p>{{selectedReward.current_points | number}}</p> </div> </md-list-item> <md-list-item class=\"md-2-line\" ng-show=\"selectedReward.type === 'POINTS'\"> <div class=\"md-list-item-text\" layout=\"column\"> <p>To Unlock</p> <p>{{selectedReward.points_thresh | number}}</p> </div> </md-list-item> </md-list> </div> </div> </div> <div class=\"modal-footer\"> <button type=\"button\" ng-click=\"openInfoModal()\" class=\"btn btn-primary\" data-dismiss=\"modal\"><i class=\"fa fa-question\"></i></button> </div> </div> </div> </div>"
  );


  $templateCache.put('views/rewards.html',
    "<div class=\"view-wrapper\"> <wave-spinner ng-if=\"brandsLoading\"></wave-spinner> <div ng-show=\"!brandsLoading\" layout=\"row\" layout-align=\"center center\"> <div class=\"card main-card\" center> <h1> <i class=\"fa fa-home pull-left\" ng-click=\"go('main')\"> </i> My Advocacy Programs </h1> <p class=\"placeholder-note\" ng-hide=\"brands.length\"> You haven't joined any advocacy programs yet </p> <div class=\"scroll-box\"> <md-list> <md-list-item ng-repeat=\"brand in brands\" class=\"list-item\" ng-click=\"go('brand-detail', {brandId: brand.brand_id})\"> <img ng-src=\"{{brand.brand_photo}}\" class=\"md-avatar\"> <h3> {{brand.brand_name}} </h3> </md-list-item> </md-list> </div> <button type=\"button\" ng-click=\"openInfoModal()\" class=\"btn btn-primary info-button\" data-dismiss=\"modal\"><i class=\"fa fa-question\"></i></button> </div> </div> </div>"
  );


  $templateCache.put('views/settings.html',
    "<div class=\"view-wrapper\"> <wave-spinner ng-if=\"profileLoading\"></wave-spinner> <div ng-show=\"!profileLoading\" layout=\"row\" layout-align=\"center center\"> <div class=\"card main-card\" center> <h1> <i class=\"fa fa-home pull-left\" ng-click=\"go('main')\"> </i> Settings </h1> <div classs=\"row\"> <div class=\"settings-wrapper\"> <div class=\"col-md-6\"> <h4> Linked Accounts </h4> <md-list class=\"social-profiles\"> <md-list-item class=\"md-2-line\"> <img ng-src=\"{{facebook.photo}}\" class=\"md-avatar\"> <div class=\"md-list-item-text\"> <h3> {{facebook.name}} </h3> <p> {{facebook.followers}} friends </p> </div> </md-list-item> <md-list-item class=\"md-2-line\"> <img ng-src=\"{{twitter.photo}}\" class=\"md-avatar\"> <div class=\"md-list-item-text\"> <h3> {{twitter.name}} </h3> <p> {{twitter.followers}} followers </p> </div> </md-list-item> <md-list-item class=\"md-2-line\"> <img ng-src=\"{{instagram.photo}}\" class=\"md-avatar\"> <div class=\"md-list-item-text\"> <h3> {{instagram.name}} </h3> <p> {{instagram.followers}} followers </p> </div> </md-list-item> <md-list-item class=\"md-2-line\"> <img ng-src=\"{{linkedin.photo}}\" class=\"md-avatar\"> <div class=\"md-list-item-text\"> <h3> {{linkedin.name}} </h3> <p> {{linkedin.followers}} connections </p> </div> </md-list-item> </md-list> <button type=\"button\" ng-hide=\"isTwitter && isInstagram && isFacebook && isTwitter\" ng-click=\"addPlatform()\" class=\"btn btn-primary\" data-dismiss=\"modal\">Link New Account</button> </div> <div class=\"col-md-6\"> <div class=\"settings-wrapper\"> <h4> My E-mails <button type=\"button\" ng-click=\"openEmailModal()\" class=\"btn btn-primary\" data-dismiss=\"modal\"> <i class=\"fa fa-plus\"> </i> </button> </h4> <md-switch title=\"Send alerts to this e-mail address\" ng-repeat=\"email in userEmails\" aria-label=\"email switch\" ng-model=\"email.isActive\" ng-change=\"updateSettings(userEmails)\"> {{email.address}} </md-switch> <h4> Alerts </h4> <md-switch title=\"Send me e-mails when new rewards are available or I unlock a reward\" ng-model=\"userSettings.reward_emails\" ng-change=\"updateSettings(userSettings)\"> New &amp; Unlocked Rewards </md-switch> <md-switch title=\"Send me e-mails when new content is available to share\" ng-model=\"userSettings.content_emails\" ng-change=\"updateSettings(userSettings)\"> New Content </md-switch> <md-switch title=\"Send me e-mails when I get a message from a rewards program\" ng-model=\"userSettings.message_emails\" ng-change=\"updateSettings(userSettings)\"> Messages </md-switch> <md-switch title=\"Send me e-mails to update me on my progress toward rewards\" ng-model=\"userSettings.progress_emails\" ng-change=\"updateSettings(userSettings)\"> Progress Reports </md-switch> </div> </div> </div> <button type=\"button\" ng-click=\"openInfoModal()\" class=\"btn btn-primary info-button\" data-dismiss=\"modal\"><i class=\"fa fa-question\"></i></button> </div> </div> </div></div>"
  );


  $templateCache.put('views/share.html',
    "<div class=\"view-wrapper\"> <wave-spinner ng-if=\"draftLoading\"></wave-spinner> <div ng-show=\"!draftLoading\" layout=\"row\" layout-align=\"center center\"> <div class=\"card share-card\" center height-adjust=\"50\"> <div class=\"card-header\" ng-style=\"selectedPlatform.color\"> <md-list-item> <img ng-show=\"isPlatform(selectedPlatform.name)\" ng-src=\"{{selectedPlatform.data.photo}}\" err-src=\"images/empty_profile.jpg\" class=\"md-avatar\"> <img ng-show=\"!isPlatform(selectedPlatform.name)\" src=\"images/empty_profile.jpg\" class=\"md-avatar\"> <p ng-show=\"isPlatform(selectedPlatform.name)\">{{selectedPlatform.data.name}}</p> <p ng-show=\"!isPlatform(selectedPlatform.name)\">No Account Linked</p> <md-input-container> <md-select ng-model=\"selectedPlatform.name\"> <md-option ng-click=\"setPlatform(platform)\" ng-repeat=\"platform in draft.platforms\" value=\"{{platform}}\"> <i class=\"fa fa-{{platform}}\"></i> {{platform | capitalize}} </md-option> </md-select> </md-input-container> </md-list-item> </div> <div class=\"card-body\"> <span ng-show=\"isPlatform(selectedPlatform.name) && !instagramSelected\"> <div class=\"card-top\" ng-click=\"contentClick()\"> <h4 class=\"card-title\">{{draft.title}}</h4> <div class=\"image-wrapper\" ng-show=\"draft.image || draft.og_image\"> <img class=\"card-img-top\" ng-src=\"{{draft.image || draft.og_image}}\"> </div> <p class=\"card-text\">{{draft.details}}</p> <p class=\"card-link\" ng-show=\"draft.long_url\">{{draft.long_url}}</p> </div> <ul class=\"list-group list-group-flush\"> <li class=\"list-group-item\" ng-show=\"draft.hashtags\"> <h3>Hashtags <span class=\"instructions\">Click to add to post</span></h3> <button ng-click=\"addToPost(hashtag)\" ng-repeat=\"hashtag in draft.hashtags\" class=\"btn btn-primary btn-sm\" ng-style=\"selectedPlatform.color\">{{hashtag}}</button> </li> <li class=\"list-group-item\" ng-show=\"draft.mentions\"> <h3>Tags</h3> <button ng-click=\"addToPost(mention)\" ng-repeat=\"mention in draft.mentions\" class=\"btn btn-primary btn-sm\" ng-style=\"selectedPlatform.color\">{{mention}}</button> </li> </ul> <textarea rows=\"4\" name=\"field\" ng-model=\"draft.text\" placeholder=\"Click buttons above to copy into post or type here. The link will automatically be added to your post. . .\" ng-change=\"linkPrefix(draft.text)\"></textarea> <div class=\"share-buttons\"> <span class=\"char-count\" ng-show=\"selectedPlatform.name === 'twitter'\" ng-class=\"{'danger' : draft.text.length + draft.short_url.length + 1 > 140}\"> {{draft.text.length + draft.short_url.length + 1}} </span> <button class=\"btn btn-primary btn-share\" ng-click=\"share(selectedPlatform.name, draft)\" ng-style=\"selectedPlatform.color\"> <i class=\"fa fa-{{selectedPlatform.name}}\"></i>&nbsp;Share </button> </div> </span> <span ng-show=\"!isPlatform(selectedPlatform.name)\"> <div class=\"card-top text-center\"> <h4 class=\"card-title show-overflow\">No {{selectedPlatform.name | capitalize}} account linked</h4> <button class=\"btn btn-primary\" style=\"font-weight: bold\" ng-style=\"selectedPlatform.color\" ng-click=\"addPlatform(selectedPlatform.name)\"> Link Account </button> </div> </span> <span ng-show=\"instagramSelected\"> <div class=\"card-top text-center\"> <h4 class=\"card-title show-overflow\">You must use the Fanzee mobile app to share to Instagram</h4> <a class=\"btn btn-primary\" style=\"font-weight: bold\" ng-style=\"selectedPlatform.color\" ng-href=\"{{brand.mobileLink}}\"> Download App </a> </div> </span> <div class=\"card-footer\"> <div class=\"row\"> <div class=\"col col-md-3 text-center\"> <span ng-click=\"go('main')\"> <i class=\"fa fa-home\"> </i> Home </span> </div> <div class=\"col col-md-3 text-center\"> <span ng-click=\"go('feed')\"> <i class=\"fa fa-newspaper-o\"> </i> Feed </span> </div> <div class=\"col col-md-3 text-center\"> <span ng-click=\"openInfoModal()\"> <i class=\"fa fa-question\"> </i> FAQ </span> </div> <div class=\"col col-md-3 text-center\"> <span> <a class=\"app-link\" ng-href=\"{{brand.mobileLink}}\"><i class=\"fa fa-mobile\"></i> Download</a> </span> </div> </div> </div> </div> </div> </div></div>"
  );

}]);
