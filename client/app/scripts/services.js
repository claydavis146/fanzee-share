'use strict';

angular.module('fanzeeShareApp')

.service('Drafts', function($firebaseArray, $firebaseObject, store, Connections, $state, $base64, auth) {
  var baseRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login page
      }
    });

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login page
        $state.go('login');
      }
    });

  var userID = $base64.encode(auth.profile.user_id);

  var draftsRef = baseRef.child(userID).child("requests");

  var drafts = $firebaseArray(draftsRef);


  var userRef = baseRef.child(userID);

  var self = this;


  this.all = function() {
            //Reload drafts from Firebase to remove locally added feed items
            drafts = $firebaseArray(draftsRef);

            return drafts.$loaded().then(function(drafts) {
              //get all of the brands the user is connected with
              return Connections.connections().then(function(connections){
                angular.forEach(connections, function(value, key) {
                  if(value['brand_id']){
                  //load the feed for each brand connected with the user
                  var feed = $firebaseArray(brandsRef.child(value['brand_id']).child('feed'));
                  feed.$loaded()
                  .then(function(feed){
                    angular.forEach(feed, function(val, k) {

                      //get the draft data
                      var d = val;

                      self.hidden().then(function(hiddenRequests){

                        //Check if this request has been hidden(deleted) by the user
                        if(Object.values(hiddenRequests).indexOf(d.$id) <= -1){

                          //console.log(d);
                          //if it hasn't been hidden, push it to the local drafts array
                          drafts.push(d);

                        };

                      })
                      
                    });

                  });
                };
              });
                return drafts
              })
              .catch(function(err){
                alert("Could not connect to server");
              });


            })
            .catch(function(err){
              alert("Could not connect to server");
            });
          }

          this.get = function(source, brandID, draftID) {
           switch(source){
            case 'id':
            //In this case, we use the request_id child of the direct request to
            //get the data from the brands requests
            var requests = $firebaseArray(brandsRef.child(brandID).child('requests'));
            return requests.$loaded().then(function(data){
              return data.$getRecord(draftID).content;
            });
          }
        };

        this.save = function(draft) {
          drafts.$save(draft);
        };

        this.delete = function(draft) {
          if (draft.audience === 'feed'){
              //add to hidden so user doesn't see this feed item anymore
              userRef.child('hidden_requests').push(draft.$id);

            }
            //remove from user drafts object
            draftsRef.child(draft.$id).remove();
          };

          this.hidden = function() {

            var hiddenRequests = $firebaseObject(userRef.child('hidden_requests'));
            return hiddenRequests.$loaded().then(function(data){
              return data
            });
          };

        })

.service('Twitter', function($http, Social, auth, Drafts, $state) {

  this.share = function(draft, imgURI, imageData) {
    var brand_id = null;

    if(draft != null && draft.brand_id != null){
      brand_id = draft.brand_id;
    }; 

    if (imgURI != null) {
      var img = {
        'image': imageData,
        'user_id': auth.profile.user_id
      };
      //upload image if there is one selected by user
      return $http.post('https://fanzee.com/twitter/upload', img)
      .then(function(data) {

        var media_id = data.data.media_id_string;

        var data = {
          'status': draft.text,
          'user_id': auth.profile.user_id,
          'short_url': draft.short_url || null,
          'long_url': draft.long_url || null,
          'request_id': draft.request_id,
          'brand_id': brand_id,
          'media_ids': media_id
        };

      //post to postmedia if there is an image uploaded
      $http.post('https://fanzee.com/twitter/postmedia', data)
      .then(function(data) {
        return data;
        })   // success
      .catch(function(err) {
        console.log(err);    
      }); 
      })   // success
      .catch(function(err) {
        console.log(err);
      }); 
    } else {
      var data = {
        'status': draft.text,
        'user_id': auth.profile.user_id,
        'short_url': draft.short_url || null,
        'long_url': draft.long_url || null,
        'request_id': draft.request_id,
        'brand_id': brand_id
      };
      //post to post endpoint if there is no image uploaded
      return $http.post('https://fanzee.com/twitter/post', data)
      .then(function(data) {
        return data;
      })
      .catch(function(err) {
        console.log(err);
      }); 
    }
  };
})

.service('Emails', function ($http, $firebaseObject, auth, $state, store, $base64) {

  var self = this;
  var profile = auth.profile;
  var influencerID = $base64.encode(profile.user_id);
  var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  this.connection = function(brandID, name) {
    var data = {
      'brand_id': brandID,
      'name': name    
    }
    $http.post('https://fanzee.com/mail/connection', data)
    .then(function() {
    })
    .catch(function(err) {
    })

  };


  this.message = function(brandID, name) {
    var data = {
      'brand_id': brandID,
      'name': name    
    }
    $http.post('https://fanzee.com/mail/message', data)
    .then(function() {
    })
    .catch(function(err) {
    })

  };

  this.userEmails = function() {
    var emails = $firebaseObject(influencersRef.child(influencerID).child('identity').child('emails'));

    return emails.$loaded()
    .then(function(result){
      return result;
    });
  };

  this.userSettings = function() {
    var settings = $firebaseObject(influencersRef.child(influencerID).child('identity').child('user_settings'));

    return settings.$loaded()
    .then(function(result){
      return result;
    });
  };



})

.service('Connections', function($firebaseArray, $firebaseObject, OpenData, $http, auth, Emails, store, $state, $base64) {

  var profile = auth.profile;
  var influencerID = $base64.encode(profile.user_id);

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
  brandsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
  influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var identity = $firebaseObject(influencersRef.child(influencerID).child("identity"));

  this.allRequests = function() {

   var connection_requests = $firebaseArray(influencersRef.child(influencerID).child('connection_requests'));
   return connection_requests;

 };

 this.connections = function() {

  var connections = $firebaseArray(influencersRef.child(influencerID).child('connected'));

  return connections.$loaded()
  .then(function(result){
    return result;
  });

};

this.brandConnections = function (brand_id) {

  return connections= $firebaseArray(brandsRef.child(brand_id).child('influencers').child('connected')).$loaded(function(data){
    return data;
  });


};

this.add = function(brandId) {

  return OpenData.brandInfo(brandId)
  .then(function(brand){
      //add connection info for brand & influencer 
      influencersRef.child(influencerID).child('connected').push({'brand_name': brand.name, 'brand_id': brandId, 'brand_photo': brand.logo});
      brandsRef.child(brandID).child('influencers').child('connected').push({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': identity.picture});
      return true
    },
    function(err){
      console.log(err);
    });
}

this.approveRequest = function(request) {

  var brandID = request.brand_id;

      //add connection info for brand & influencer 
      influencersRef.child(influencerID).child('connected').push({'brand_name': request.brand_name, 'brand_id': request.brand_id, 'brand_photo': request.brand_photo});
      brandsRef.child(brandID).child('influencers').child('connected').push({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': identity.picture});

      //remove from requests for brand & influencer
      influencersRef.child(influencerID).child('connection_requests').child(request.$id).remove();
      brandsRef.child(brandID).child('influencers').child('requested').child(request.request_key).remove();

      //send email notification
      Emails.connection(request.brand_id, profile.name);
    };
  })

.service('Social', function (auth, $http, $state, store) {

 var prof = auth.profile;     

 this.profiles = {
  'facebook':{},
  'twitter':{},
  'instagram':{},
  'linkedin':{}
};
    var self = this; //passes current "this" to http.get

    this.isTwitter = function() {
      for (var i = 0; i < prof.identities.length; i++) {
        if (angular.equals(prof.identities[i].provider, "twitter")) {
         return true;
       }
     }
     return false;
   };

   this.isInstagram = function() {
    for (var i = 0; i < prof.identities.length; i++) {
      if (angular.equals(prof.identities[i].provider, "instagram")) {
       return true;
     }
   }
   return false;
 };

 this.isFacebook = function() {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "facebook")) {
     return true;
   }
 }
 return false;
}; 

this.isLinkedIn = function() {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "linkedin")) {
     return true;
   }
 }
 return false;
};

auth.getProfile().then(function(profile) {
  self.rebuildIdentities(profile);
});

this.rebuildIdentities = function (prof) {
  for (var i = 0; i < prof.identities.length; i++) {
    if (angular.equals(prof.identities[i].provider, "facebook")) {
      var data = {
        'user_id': prof.user_id     
      };
      $http.post('https://fanzee.com/facebook/verify', data)
      .then(function(data) {

        self.profiles.facebook.followers = data.data.friends;
        self.profiles.facebook.name = data.data.name;
        self.profiles.facebook.photo = data.data.photo;
        if(data.data.name == undefined) {
          self.tokenRefresh('facebook');
        };
      })
      .catch(function(err) {
        //handle error
      })
      $http.post('https://fanzee.com/facebook/pages-list', data)
      .then(function(data) {

        if(data.data.data) {
          self.profiles.facebook.pages = data.data.data;
        }

        if(data.data.error) {
          self.tokenRefresh('facebook');
        };
      })
      .catch(function(err) {
        //handle error
        console.log(err);
      })

    } 
    if (angular.equals(prof.identities[i].provider, "twitter")) {
     var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/twitter/verify', data)
    .then(function(data) {

      self.profiles.twitter.followers = data.data.followers_count;
      self.profiles.twitter.name = "@" + data.data.screen_name;
      self.profiles.twitter.photo = data.data.profile_image_url_https;
      if(data.data.screen_name == undefined) {
        self.tokenRefresh('twitter');
      };
    })
    .catch(function(err) {
      //handle error
    }) 
  }
  else if (angular.equals(prof.identities[i].provider, "instagram")) {

    var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/instagram/verify', data)
    .then(function(data) {

      self.profiles.instagram.followers =  data.data.counts.followed_by;
      self.profiles.instagram.name = "@" + data.data.username;
      self.profiles.instagram.photo = data.data.profile_picture;
      if(data.data.username == undefined) {
        self.tokenRefresh('instagram');
      };
    })
    .catch(function(err) {
      //handle error
    })
  }
  else if (angular.equals(prof.identities[i].provider, "linkedin")) {

    var data = {
      'user_id': prof.user_id     
    };
    $http.post('https://fanzee.com/linkedin/verify', data)
    .then(function(data) {
      if(data.data.numConnectionsCapped == true){
        self.profiles.linkedin.followers = '500+';
      } else {
        self.profiles.linkedin.followers = data.data.numConnections;
      }
      self.profiles.linkedin.name =  data.data.firstName + ' ' + data.data.lastName;
      self.profiles.linkedin.photo = data.data.pictureUrl;
      if(data.data.numConnectionsCapped == undefined) {
        self.tokenRefresh('linkedin');
      };
    })
    .catch(function(err) {
      console.log(err);
      //handle error
    })
  }
};
};

this.tokenRefresh = function(platform) {
  var lock = new Auth0Lock('ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x', 'fanzee.auth0.com');
  lock.show({
    closable: true,
    icon: 'https://fanzee.com/images/white-menu-logo.ea2497ed.png',
    dict: {
      signin: {
        title: 'Please reverify your ' + platform + ' account'
      }
    },
    connections: [platform],
    authParams: {
     connection_scopes: {
      'linkedin': ['r_basicprofile', 'w_share']
    }
  }
}, function(err, profile, id_token){
  if(err){ 
    alert('Something went wrong.  Please try again!');
  }
  else {
    store.set('profile', profile);
    prof = profile;
    self.rebuildIdentities(profile);
  }
})
}

this.linkNewAccount = function () {

  var platforms = ['twitter', 'instagram', 'linkedin', 'facebook'];
    //remove already connected platforms
    if(self.isInstagram()) {
      var index = platforms.indexOf('instagram');
      if(index!=-1){
       platforms.splice(index, 1);
     }
   };
   if(self.isTwitter()) {
    var index = platforms.indexOf('twitter');
    if(index!=-1){
     platforms.splice(index, 1);
   }
 };
 if(self.isLinkedIn()) {
  var index = platforms.indexOf('linkedin');
  if(index!=-1){
   platforms.splice(index, 1);
 }
};
if(self.isFacebook()) {
  var index = platforms.indexOf('facebook');
  if(index!=-1){
   platforms.splice(index, 1);
 }
};

var lock = new Auth0Lock('ywxuAvQN38sQqiiwYz3CJ3IsPUiEg94x', 'fanzee.auth0.com');
lock.show({
  dict: {
    signin: {
      title: 'Link another account'
    }
  },
  icon: 'https://fanzee.com/images/white-menu-logo.ea2497ed.png',
  rememberLastLogin: false,
  connections: platforms,
  authParams: {
   connection_scopes: {
    'linkedin': ['r_basicprofile', 'w_share']
  }
}
}, function(err, profile, id_token){
  $http({
    method: "POST",
    headers: {'Authorization': 'Bearer '+ store.get('token') },
    url: "https://fanzee.auth0.com/api/v2/users/" + prof.user_id + "/identities",
    data: {
      'link_with': id_token
    }
  })
  .then(function(response) {

    auth.getProfile().then(function(profile){
      store.set('profile', profile);
      prof = profile;
      self.rebuildIdentities(profile);
      $state.go('main', {reload: true});
    });

  })
  .catch(function(err) {
    $state.go('login', {reload: true});
  })  
})
};
})

.service('OpenData', function ($http, auth, $state, $base64, $firebaseArray, store) {

  var self = this;

  this.brandInfo = function (brand_id) {
    var url = 'https://fanzee.com/open/brand-info/' + brand_id;
    return $http.get(url)
    .then(function(response){
      var brand = response.data;
      var linkData = {
        name: response.data.name,
        id: brand_id,
        photo: response.data.logo,
        widget_messages: response.data.widget_messages
      }
      self.getDeeplink(linkData).then(function(result){
       brand.mobileLink = result;
     })
      return brand
    });
  }

  //Alternative connection function so we don't have to include the Connections service before user is logged in
  this.addConnection = function(brandId) {

    var profile = auth.profile;
    var influencerID = $base64.encode(profile.user_id);

    var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands/");
    brandsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
      if (error) {
        $state.go('login');
      }
    });

    var influencersRef = new Firebase("https://shining-inferno-4546.firebaseio.com/users/mobile/");
    influencersRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
      if (error) {
        $state.go('login');
      }
    });

    var connections = $firebaseArray(influencersRef.child(influencerID).child('connected'));

    return connections.$loaded()
    .then(function(result){
      for (var i = 0; i < result.length; i++){
       if (result[i].brand_id === brandId){
        return
      }
    }
    return self.brandInfo(brandId)
    .then(function(brand){
        //add connection info for brand & influencer 
        influencersRef.child(influencerID).child('connected').child('brand-' + brandId).set({'brand_name': brand.name, 'brand_id': brandId, 'brand_photo': brand.logo});
        brandsRef.child(brandId).child('influencers').child('connected').child('adv-' + influencerID).set({'influencer_name': profile.name, 'influencer_id': influencerID, 'influencer_photo': profile.picture});
        return
      },
      function(err){
        return err
        console.log(err);
      });
  });
  }

  this.getDeeplink = function (brand) {
    var linkData = {
      'brand': {
        name: brand.name,
        id: brand.id,
        photo: brand.photo
      }
    };
    return $http.post('https://fanzee.com/deeplink/invite', linkData)
    .then(function (data) {
      return data.data;
    })
    .catch(function (err) {
      console.log(err);
    });
  }

})

.service('LinkedIn', function (auth, $http, $state, store, Social, Drafts) {
  this.share = function(draft) {
   var brand_id = draft.brand_id;

   if(draft.short_url)
   {
    var share_url = draft.short_url;
  } else {
    var share_url = draft.long_url;
  }

  var data = {
    'comment': draft.text || null,
    'user_id': auth.profile.user_id,
    'share_url': share_url,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url,
    'og_image': draft.og_image || null,
    'request_id': draft.request_id,
    'brand_id': brand_id
  };

  return $http.post('https://fanzee.com/linkedin/post', data)
  .then(function(data) {
    return data;
  })
  .catch(function(err) {
    console.log(err);
    return err;
  }); 
};

})

.service('Facebook', function (auth, $http, $state, store, Social, Drafts) {

  this.shareText = function(draft) {

    var brand_id = draft.brand_id;

    var data = {
      'comment': draft.text,
      'user_id': auth.profile.user_id,
      'request_id': draft.request_id,
      'brand_id': brand_id,
      'short_url': draft.short_url || null,
      'long_url': draft.long_url || null
    };

    return $http.post('https://fanzee.com/facebook/post', data)
    .then(function(data) {
      return data;

    })
    .catch(function(err) {

      return err;
    }); 

  };

  this.shareImageURL = function(draft, imageURL) {

   var brand_id = draft.brand_id;

   var data = {
    'caption': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'image_uri': imageURL,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  };

  return $http.post('https://fanzee.com/facebook/image-upload-by-link', data)
  .then(function(data) {
    return data;

  })
  .catch(function(err) {
    alert(JSON.stringify(err));
    return err;
  }); 
};

this.shareImageURLPage = function(draft, page_id, imageURL) {

  var brand_id = draft.brand_id;

  var data = {
    'caption': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'image_uri': imageURL,
    'page_id': page_id,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  };

  return $http.post('https://fanzee.com/facebook/pages/image-upload-by-link', data)
  .then(function(data) {
    return data;

  })
  .catch(function(err) {
    alert(JSON.stringify(err));
    return err;
  }); 
};

this.shareTextPage = function(draft, page_id) {

  var brand_id = draft.brand_id;

  var data = {
    'comment': draft.text,
    'user_id': auth.profile.user_id,
    'request_id': draft.request_id,
    'brand_id': brand_id,
    'page_id': page_id,
    'short_url': draft.short_url || null,
    'long_url': draft.long_url || null
  }; 

  return $http.post('https://fanzee.com/facebook/pages/post', data)
  .then(function(data) {
    return data;
  })
  .catch(function(err) {
    console.log(err);
    return err;
  }); 
};

})

.service('LinkedIn', function (auth, $http, $state, store, Social, Drafts) {


  this.share = function(draft) {

    if(draft != null && draft.brand_id != null){
     var brand_id = draft.brand_id;

     if(draft.short_url)
     {
      var share_url = draft.short_url;
    } else {
      var share_url = draft.long_url;
    }

    var data = {
      'comment': draft.text || null,
      'user_id': auth.profile.user_id,
      'share_url': share_url,
      'short_url': draft.short_url || null,
      'long_url': draft.long_url,
      'og_image': draft.og_image || null,
      'request_id': draft.request_id,
      'brand_id': brand_id
    };

    return $http.post('https://fanzee.com/linkedin/post', data)
    .then(function(data) {
      return data;
    })
    .catch(function(err) {
      console.log(err);
      return err;
    }); 
  } 
};
})

.service('ShortLinks', function ($http, $base64, auth) {
  var self = this;

  var userID = $base64.encode(auth.profile.user_id);

  this.shorten = function(url) {

    var data = {
      'url':  url + '?fzuser=' + userID
    }

  //shorten link so clicks can be tracked
  return $http.post('https://fanzee.com/google/shorten', data)
  .then(function(response) {
    return response.data;
  })
  .catch(function(err) {
    console.log(err);
  })

};

})

.service('Leaderboards', function ($http, $base64, auth, $firebaseArray, $firebaseObject, store) {

  var self = this;

  var leaderboardsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/leaderboards/");
  leaderboardsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login state
        $state.go('login');
      }
    });

  this.getLeaderboard = function(brandID, leaderboardID) {

    if(leaderboardID === 'all_time_leaderboard' || leaderboardID === 'monthly_leaderboard' || leaderboardID === 'weekly_leaderboard') {
      var board = $firebaseObject(leaderboardsRef.child(brandID).child(leaderboardID));
    }
    else {
      //Need function to get list of custom leaderboards and another to create a fb object for a particular one by $id
      var board = $firebaseObject(leaderboardsRef.child(brandID).child('custom').child(leaderboardID));
    }

    return board.$loaded(function(data) {

       return data; // true
     },
     function(error) {
      console.error("Error:", error);
    });

  };

  this.listCustom = function(brandID) {


    var customLeaderboards = $firebaseArray(leaderboardsRef.child(brandID).child('custom'));

    return customLeaderboards.$loaded(function(data){
      return data;
    });

  };

  this.getUserRankings = function (user_id) {
   return $http({
    url: 'https://fanzee.com/advocate/' + user_id + '/rankings-view' , 
    method: "GET"
  }).then(function(data){
    return data;
  }).catch(function(err) {
    console.log(err);
    return err;
  }); 
}

})

.service('CloudinaryUpload', function () {

  this.uploadImage = function(imgURI) {

    var server = 'https://api.cloudinary.com/v1_1/fanzee/image/upload';

    var options = {
      'params': {
        'upload_preset': 'twlaajzw'
      }
    };

      /*return $cordovaFileTransfer.upload(server, imgURI, options)
      .then(function(result) {

        var response = JSON.parse(result.response);

        return response.secure_url;
        // Success!
      }, function(err) {
        console.log(JSON.stringify(err));
        return err;
        // Error
      });*/

      
    };
  })

.service('Messages', function($firebaseArray, $firebaseObject, auth, store, $state, Emails, $base64, Connections) {

  var baseRef = new Firebase("https://shining-inferno-4546.firebaseio.com/");
  baseRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
      $state.go('login');
    }
  });

  var brands = $firebaseArray(baseRef.child('brands'));

  var fb_id = $base64.encode(auth.profile.user_id);

  var messageRoomsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/message_rooms");
  var identity = $firebaseObject(baseRef.child("users").child("mobile").child(fb_id).child("identity"));

  var brandsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/brands");

  var self = this;

  this.selectedRoom = null;
  var messageRooms = messageRoomsRef.orderByChild("influencer").equalTo(fb_id);

  this.messageRooms = function() {
    return $firebaseArray(messageRooms);
  };

  this.brands = function() {
    var connectedBrands = Connections.connections();
    return connectedBrands.$loaded()
    .then(function(result){
      return result;
    });
  };

  this.getBrand = function(brandID) {
    return brands.$loaded().then(function(data){
      return data.$getRecord(brandID);
    });
  };

  this.getRoom = function(roomID) {
    var rooms = self.messageRooms();
    return rooms.$loaded().then(function(data){
      return data.$getRecord(roomID);
    });
  };

  this.load = function (roomId) {
    var messages = $firebaseArray(messageRoomsRef.child(roomId).child('messages'));
    return messages.$loaded().then(function(messageArray){
      return messageArray
    });
  };

  this.setRoom = function(room) {
    self.selectedRoom = room;
  };

  this.createRoom = function(brandID) {
    var newRoom = {};
    newRoom.influencer = $base64.encode(auth.profile.user_id);
    newRoom.brand = brandID;
    var newRoomRef = messageRoomsRef.push(newRoom);
    var selected = newRoomRef.key();
    self.selectedRoom = self.getRoom(selected);
  };

  this.add = function(newMessage, userName, userImage) {

    messageRoomsRef.child(self.selectedRoom.$id).child('messages').push({ 'content': newMessage, 'from': userName, 'image': identity.picture });
    Emails.message(self.selectedRoom.brand, userName);
    self.alertBrand();

  };

  this.alertBrand = function() {;
    var brand = self.selectedRoom.brand
    var influencer = self.selectedRoom.influencer;

    var alert = $firebaseArray(brandsRef.child(brand).child("influencers").child("connected").orderByChild("influencer_id").equalTo(influencer)).$loaded().then(function(alert){

      var connection_id = alert[0].$id;
      brandsRef.child(brand).child("influencers").child("connected").child(connection_id).update({
        "new_message": "true"
      });


    });
  };

  this.clearAlert = function(connection) {

    baseRef.child("users").child("mobile").child(fb_id).child("connected").child(connection).update({
      "new_message": "false"
    });

  };
})

.service('Rewards', function ($http, $base64, auth, $firebaseArray, $firebaseObject, store) {

  var self = this;

  var rewardsRef = new Firebase("https://shining-inferno-4546.firebaseio.com/rewards/");
  rewardsRef.authWithCustomToken(store.get('firebaseToken'), function(error, auth) {
    if (error) {
        // There was an error logging in, redirect the user to login state
        $state.go('login');
      }
    });

  this.getBrandRewards = function(brand_id) {
    var rewards = $firebaseArray(rewardsRef.child(brand_id));
    return rewards.$loaded()
    .then(function(result){
      return result;
    });
  };
});
