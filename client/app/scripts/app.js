'use strict';

/**
 * @ngdoc overview
 * @name fanzeeShareApp
 * @description
 * # fanzeeShareApp
 *
 * Main module of the application.
 */
 angular
 .module('fanzeeShareApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ui.router', 
  'ui.router.state.events',
  'ngMaterial',
  'auth0',
  'angular-storage',
  'angular-jwt',
  'ngFileUpload',
  'cloudinary',
  'firebase',
  'base64',
  'luegg.directives',
  'ngMessages',
  'angular-spinkit',
  'mailchimp',
  'ngclipboard',
  'ngToast',
  'angularMoment',
  'uiCropper',
  'yaru22.angular-timeago',
  'angularModalService'
  ])
 .config(function ($urlRouterProvider, $locationProvider, authProvider, $httpProvider, ngToastProvider, jwtInterceptorProvider, $stateProvider, cloudinaryProvider) {



  $locationProvider.html5Mode(true);

  $stateProvider

  .state('main', {
    url:'/',
    templateUrl: 'views/main.html',
    controller: 'MainCtrl'
  })

  .state('landing', {
    url:'/landing/:brandId',
    templateUrl: 'views/landing.html',
    controller: 'LandingCtrl'
  })

  .state('share', {
    url:'/share/:brandId/:sourceType/:sourceData',
    templateUrl: 'views/share.html',
    controller: 'ShareCtrl',
    data: {
      requiresLogin: true
    },
    cache: false
  })

  .state('feed', {
    url:'/feed',
    templateUrl: 'views/feed.html',
    controller: 'FeedCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('rewards', {
    url:'/rewards',
    templateUrl: 'views/rewards.html',
    controller: 'RewardsCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('brand-detail', {
    url:'/rewards/brand/:brandId',
    templateUrl: 'views/brand-detail.html',
    controller: 'RewardsCtrl',
    data: {
      requiresLogin: true
    },
    params: {
        brandId: null
    }
  })

  .state('settings', {
    url:'/settings',
    templateUrl: 'views/settings.html',
    controller: 'SettingsCtrl',
    data: {
      requiresLogin: true
    }
  })

  .state('login', {
    url: '/login/:brandId',
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl',
    params: { brandId: null}
  });

  $urlRouterProvider.otherwise('/');

  authProvider.init({
    domain: AUTH0_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    loginState: 'login'
  });
  
  jwtInterceptorProvider.tokenGetter = ['store', function(store) {
    // Return the saved token
    return store.get('token');
  }];

  $httpProvider.interceptors.push('jwtInterceptor');
  
  ngToastProvider.configure({
    animation: 'slide',
    verticalPosition: 'top',
    horizontalPosition: 'center' 
  });

  cloudinaryProvider
  .set("cloud_name", "fanzee")
  .set("upload_preset", "hcc3xwxk");


})

 .run(function($rootScope, auth, $stateParams, store, $state, $timeout, jwtHelper, $location) {

  function clean(obj) {
    for (var propName in obj) { 
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
  }

  // This events gets triggered on refresh or URL change
  auth.hookEvents();
  //This event gets triggered on URL change
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
    if (!auth.isAuthenticated) {
      var token = store.get('token');
      var refreshToken = store.get('refreshToken');
      //store original url for redirect if not logged in
      if(!$location.path().includes('login') && $location.path() != '/'){
        $rootScope.redirectData = {
          brandId: toParams.brandId,
          sourceData: toParams.sourceData,
          sourceType: toParams.sourceType,
          redirectURL: $location.path()
        }
        clean($rootScope.redirectData);
      }
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          auth.authenticate(store.get('profile'), token).then(function(prof){
            if($rootScope.redirectData && $rootScope.redirectData.redirectURL){
              $location.path($rootScope.redirectData.redirectURL);
            }
          });
        } else {
          if (refreshToken) {
            return auth.refreshIdToken(refreshToken).then(function(idToken) {
              store.set('token', idToken);
              auth.authenticate(store.get('profile'), idToken).then(function(prof){
            if($rootScope.redirectData.redirectURL){
              $location.path($rootScope.redirectData.redirectURL);
            }
          });
            });
          } else {
            auth.signout();
            store.remove('token');
            store.remove('profile');
            store.remove('refreshToken');
            store.remove('firebaseToken');
            $state.go('login');
          }                          
        }
      }
    }
    else {
      if(auth.isAuthenticated && !auth.profile.identities[0].isSocial){
        if(!$location.path().includes('/login') && $location.path() != '/'){
          $rootScope.redirectData = {
            brandId: toParams.brandId,
            sourceData: toParams.sourceData,
            sourceType: toParams.sourceType,
            redirectURL: $location.path()
          }
          clean($rootScope.redirectData);
          console.log($rootScope.redirectData);
        }
        auth.signout();
        store.remove('token');
        store.remove('profile');
        store.remove('refreshToken');
        store.remove('firebaseToken');
        $state.go('login');
      };
    };
  });


  //broadcast state changes for spinners
  $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams, options){ 
      $rootScope.stateLoading = true;
    });
  $rootScope.$on('$stateChangeSuccess', 
    function(event, toState, toParams, fromState, fromParams){ 
      $timeout(function(){
        $rootScope.stateLoading = false;
      });

    });
})

 .directive('resize', function ($window) {
  return function (scope, element) {
    var w = angular.element($window);
    var changeHeight = function() {element.css('height', (w.height()) + 'px' );};  
    w.bind('resize', function () {        
              changeHeight();   // when window size gets changed             
            });  
        changeHeight(); // when page loads          
      };
    })

 .directive('center', function ($window, $timeout) {
  return function (scope, element, attrs) {
    var win = angular.element($window);
    var changeCss = function() {
    var adjustment = attrs.heightAdjust || 0;
    var elemHeight = element[0].offsetHeight;
      if(win.height() > elemHeight + adjustment){
        element.css('position', 'absolute' );
        element.css('top', '50%');
        element.css('left', '50%');
        element.css('transform', 'translate(-50%, -50%)');
      }
      else {
        element.css('position', 'relative' );
        element.css('top', '0');
        element.css('left', '0');
        element.css('transform', 'none');
      }
    };  
    win.bind('resize', function () {       
              changeCss();   // when window size gets changed             
            }); 
            $timeout(function() {
    changeCss(); // when page loads
})           
      };
    })

 .directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
})

//compile dynamic templates
.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
})

.filter("as", function($parse) {
  return function(value, context, path) {
    return $parse(path).assign(context, value);
  };
})

.filter('numKeys', function() {
  return function(object) {
    if(object){
      return Object.keys(object).length;
    }
    else {
      return 0;
    }
  }
})

.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});
