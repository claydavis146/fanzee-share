'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the fanzeeShareApp
 */
angular.module('fanzeeShareApp')
  .controller('FeedCtrl', function ($scope, $state, ModalService, Drafts, auth) {

  	$scope.feedLoading = true;
	Drafts.all()
	  .then(function(drafts){
	   	$scope.drafts = drafts;
	   	$scope.feedLoading = false;
	  },
	  function(err){
	  	$scope.feedLoading = false;
	   	$state.go('login');
	  })

	  $scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj);
 		}
 		else{
 			$state.go(state);
 		}
 	}

 	$scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

  });
  