'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fanzeeShareApp
 */
angular.module('fanzeeShareApp')
  .controller('MainCtrl', function ($scope, $state, auth, ModalService) {

    $scope.loggedIn = false;

  	$scope.go = function(state){
  		$state.go(state);
  	}

    $scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

    auth.getProfile().then(function(data){
      $scope.loggedIn = true;
    },
    function(err){
      $scope.loggedIn = false;
      $state.go('login');
    });
   
  })
  .controller('MenuCtrl', function($timeout, $scope, $state, $mdSidenav) {
     $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    };

    $scope.goTo = function(state){
    	$state.go(state);
	};
});
  
