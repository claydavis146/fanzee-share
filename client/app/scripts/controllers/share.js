'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:ShareCtrl
 * @description
 * # ShareCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('ShareCtrl', function ($scope, $state, ModalService, OpenData, $mdDialog, Twitter, Facebook, LinkedIn, $stateParams, $window, $timeout, ShortLinks, Drafts, auth, Social) {

   $scope.$watch(
    function(){ return auth.profile},
    function(newVal) {
      $scope.profile = newVal;
      $scope.buildProfile();
    }, true
    );

   $scope.$watch(
    function(){ return Social.profiles},
    function(newVal) {
      $scope.buildProfile();
    },
    true
    );

   $scope.openInfoModal = function() {
    ModalService.showModal({
      templateUrl: "views/info-modal.html",
      controller: "MainCtrl"
    }).then(function(modal) {
      modal.element.modal();
    });
    }

   $scope.draft = {text: null};

   $scope.draftLoading = true;

   $scope.buildProfile = function (){
    $scope.instagram = Social.profiles.instagram;
    $scope.twitter = Social.profiles.twitter; 
    $scope.facebook = Social.profiles.facebook; 
    $scope.linkedin = Social.profiles.linkedin;
    $scope.isTwitter =  Social.isTwitter();
    $scope.isFacebook = Social.isFacebook();
    $scope.isInstagram = Social.isInstagram();
    $scope.isLinkedIn = Social.isLinkedIn();
  }

  $scope.isPlatform = function(platform) {
    switch(platform){
      case 'twitter':
        return $scope.isTwitter
        break
      case 'linkedin':
        return $scope.isLinkedIn
        break
      case 'facebook':
        return $scope.isFacebook
        break
      case 'instagram':
        return $scope.isInstagram
    }
  }

  $scope.buildProfile();

  $timeout(function(){
    OpenData.brandInfo($stateParams.brandId)
        .then(function(info){
          $scope.brand = info;
        },
        function(err){
          console.log(err);
        });

    Drafts.get($stateParams.sourceType, $stateParams.brandId, $stateParams.sourceData).then(function(draft){
      $scope.draft = draft;
      if($scope.draft.audience === 'feed' || $scope.draft.audience === 'link'){
        $scope.draft.request_id = draft.$id;
      };

      if ($scope.draft.link) {
        $scope.draft.long_url = $scope.draft.link;
        try {
          ShortLinks.shorten($scope.draft.link).then(function (data) {
            $scope.draft.short_url = data;
          });
        } catch (Exception) {
          console.log(Exception);
        }
      }
      $scope.draftLoading = false;
      reorderPlatforms();
    }, function(err){
      $scope.draftLoading = false;
    });
  });

  //Put synced platforms first in order 
  function reorderPlatforms() {
    if(!$scope.isInstagram && $scope.draft.platforms.indexOf('instagram') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('instagram'), 1)[0]);
    }
    if(!$scope.isFacebook && $scope.draft.platforms.indexOf('facebook') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('facebook'), 1)[0]);
    } 
    if(!$scope.isLinkedIn && $scope.draft.platforms.indexOf('linkedin') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('linkedin'), 1)[0]);
    }
    if(!$scope.isTwitter && $scope.draft.platforms.indexOf('twitter') > -1) {
      $scope.draft.platforms.push($scope.draft.platforms.splice($scope.draft.platforms.indexOf('twitter'), 1)[0]);
    }
    $scope.setPlatform($scope.draft.platforms[0]);
  }

  $scope.addPlatform = function(platform){
  //please link X platform alert
  Social.linkNewAccount();
  }

$scope.addToPost = function(content) {
    if($scope.draft.text == null || $scope.draft.text == '') {
      $scope.draft.text = content;
    } else {
      $scope.draft.text += " "+ content + " ";
    }
    //NgToast 'added to post!'
  }

$scope.share = function(platform, draft) {

   var alert = $mdDialog.alert()
            .title('Too many characters!')
            .content('Tweets must be 140 characters or less')
            .ariaLabel('Twitter warning')
            .ok('Ok');

  switch(platform){
    case 'twitter':
      if (draft.short_url){
        if(!draft.text){
          draft.text = null;
        }
        draft.text = draft.text + ' ' + draft.short_url;
      }
      if(draft.text.length > 140){
        $mdDialog.show(alert);
        return
      }
      Twitter.share(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      });
      break
    case 'facebook':
      Facebook.shareText(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      });
      break
    case 'linkedin':
      LinkedIn.share(draft).then(function(){
        handleShared();
      }, function(err) {
        // Error
      }); 
  }
}

function handleShared() {
 var index = $scope.draft.platforms.indexOf($scope.selectedPlatform.name);
 $scope.draft.text = '';
   var alert = $mdDialog.alert()
            .title('Thanks for sharing!')
            .content('Keep sharing to earn more points and unlock rewards!  Simply close this window when you are finished.')
            .ariaLabel('Thanks for sharing')
            .ok('Ok');

      $mdDialog.show(alert).then(function () {
        if(index === $scope.draft.platforms.length - 1 &&  $scope.draft.platforms.length != 1){
            $scope.setPlatform(scope.draft.platforms[index - 1]);
            return
          }
          if($scope.draft.platforms.length != 1){
          $scope.setPlatform($scope.draft.platforms[index + 1]);
          return
        }
        return
        });
};

$scope.contentClick = function() {
  if($scope.draft.long_url){
   var confirm = $mdDialog.confirm()
            .title('What would you like to do?')
            .content('You can copy the preview text into your post or view this link in your browser')
            .ariaLabel('Open Link')
            .ok('Open Link')
            .cancel('Copy Text')
            .clickOutsideToClose(true);

      $mdDialog.show(confirm).then(function(res) {
        $window.open($scope.draft.long_url, '_blank');
    }, function() {
    $scope.addToPost($scope.draft.details);
  });
  }
  else {
    return
  }
}

$scope.setPlatform = function(platform) {
  $scope.selectedPlatform = {};
  $scope.selectedPlatform.name = platform;
  switch(platform){
    case 'twitter':
      $scope.selectedPlatform.data = $scope.twitter;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#00aced'};
    break
    case 'facebook':
      $scope.selectedPlatform.data = $scope.facebook;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#3B5998'};
    break
    case 'linkedin':
      $scope.selectedPlatform.data = $scope.linkedin;
      $scope.instagramSelected = false;
      $scope.selectedPlatform.color = {'background-color': '#0077b5'};
    break
    case 'instagram':
      $scope.selectedPlatform.data = $scope.instagram;
      $scope.instagramSelected = true;
      $scope.selectedPlatform.color = {'background-color': '#bc2a8d'};
  }
}

  $scope.go = function(state, paramObj){
    if(paramObj){
      $state.go(state, paramObj)
    }
    else{
      $state.go(state);
    }
  }

});
