'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('SettingsCtrl', function ($scope, $state, ModalService, auth, $base64, Emails, Social) {

 	$scope.$watch(
 		function(){ return auth.profile},
 		function(newVal) {
 			$scope.profile = newVal;
 			$scope.buildProfile();
 			$scope.profileLoading = true;
 			Emails.userEmails()
 			.then(function(emails){
 				$scope.userEmails = emails;
 				Emails.userSettings()
 				.then(function(settings){
 					$scope.userSettings = settings;
 					$scope.profileLoading = false;
 				},
 				function(error){
 					$scope.profileLoading = false;
 				})
 			}, function(err){
 				$scope.profileLoading = false;
 			})
 		}
 		);

 	$scope.$watch(
 		function(){ return Social.profiles},
 		function(newVal) {
 			$scope.buildProfile();
 		},
 		true
 		);

 	$scope.buildProfile = function (){
 		$scope.instagram = Social.profiles.instagram;
 		$scope.twitter = Social.profiles.twitter; 
 		$scope.facebook = Social.profiles.facebook; 
 		$scope.linkedin = Social.profiles.linkedin;
 		$scope.isTwitter =  Social.isTwitter();
 		$scope.isFacebook = Social.isFacebook();
 		$scope.isInstagram = Social.isInstagram();
 		$scope.isLinkedIn = Social.isLinkedIn();
 	}

 	$scope.buildProfile();

 	$scope.isPlatform = function(platform) {
 		switch(platform){
 			case 'twitter':
 			return $scope.isTwitter
 			break
 			case 'linkedin':
 			return $scope.isLinkedIn
 			break
 			case 'facebook':
 			return $scope.isFacebook
 			break
 			case 'instagram':
 			return $scope.isInstagram
 		}
 	}

 	$scope.updateSettings = function(object){
 		object.$save();
 	}

 	$scope.addEmail = function(address){
 		var addressKey = $base64.encode(address);
 		$scope.userEmails[addressKey] = {
 			address: address,
 			isActive: true
 		}
 		$scope.updateSettings($scope.userEmails);
 	}

 	$scope.addPlatform = function(platform){
 		Social.linkNewAccount();
 	}

 	$scope.openInfoModal = function() {
 		ModalService.showModal({
 			templateUrl: "views/info-modal.html",
 			controller: "MainCtrl"
 		}).then(function(modal) {
 			modal.element.modal();
 		});
 	}

 	$scope.openEmailModal = function() {
 		ModalService.showModal({
 			templateUrl: "views/add-email-modal.html",
 			controller: "SettingsCtrl",
 			scope: $scope
 		}).then(function(modal) {
 			modal.element.modal();
 		});
 	}

 	$scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj)
 		}
 		else{
 			$state.go(state);
 		}
 	}

 });
