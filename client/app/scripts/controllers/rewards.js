'use strict';

/**
 * @ngdoc function
 * @name fanzeeShareApp.controller:RewardsCtrl
 * @description
 * # RewardsCtrl
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller('RewardsCtrl', function ($scope, auth, $base64, $filter, ModalService, $state, Messages, Connections, Rewards, $stateParams, Leaderboards) {

 	$scope.go = function(state, paramObj){
 		if(paramObj){
 			$state.go(state, paramObj)
 		}
 		else{
 			$state.go(state);
 		}
 	}

  $scope.message = {text: null};
 	$scope.messageRooms = Messages.messageRooms();
 	$scope.brandsLoading = true;
 	$scope.showMessages = false;
 	$scope.user_id = $base64.encode(auth.profile.user_id);

 	Connections.connections()
 	.then(function(connections){
 		$scope.brands = connections;
 		$scope.brandsLoading = false;
 	},
 	function(err){
 		$scope.brandsLoading = false;
 	});

 	$scope.$watch(
 		function(){ return $stateParams},
 		function(newVal) {
 			if($stateParams.brandId){
 				$scope.brandLoading = true;
 				$scope.currentTime = new Date().getTime();
 				$scope.setRoom($stateParams.brandId);
 				Messages.getBrand($stateParams.brandId).then(function(data){
 					$scope.selectedBrand = data;
 					$scope.brandLoading = false;
 				},
 				function(err){
 					$state.go('rewards');
 				});
 				$scope.setRoom($stateParams.brandId);
 				loadRewards($stateParams.brandId);
 			};
 		}, true
 		);

 	$scope.setRoom = function(brandID) {
 		for(var i=0, len = $scope.messageRooms.length; i < len; i++){
 			if ($scope.messageRooms[i].brand == brandID) {
 				Messages.setRoom($scope.messageRooms[i]);
        Messages.load($scope.messageRooms[i].$id).then(function(array){
          $scope.messagesArray = array;
          return;
        }); 
        return
 			} 
 		}
 		Messages.createRoom(brandID);
 	}

  $scope.sendMessage = function() {
     if($scope.message.text){
      Messages.add($scope.message.text, auth.profile.name, auth.profile.picture);
      $scope.message.text = null;
    }
  }

  $scope.copiedAlert = function() {
    var alert = $mdDialog.alert()
            .title('Code copied')
            .content('The coupon code has been copied to your clipboard')
            .ariaLabel('Copied alert')
            .ok('Ok');
            $mdDialog.show(alert);
  }

 	function loadRewards(brandID){
 		$scope.rewardsLoading = true;
 		$scope.userRewards = {};
 		Rewards.getBrandRewards(brandID)
 		.then(function(rewards){
 		$scope.rewards = rewards;
          //Get relative rankings for each leaderboard
          Leaderboards.getUserRankings($scope.user_id)
          .then(function(rankings){
          	var rankings = rankings.data;
          	for (var i = 0; i < rewards.length; i++){
              //get ranking for leaderboard and append it to reward object for display
              if($scope.rewards[i].leaderboard_ref){
              	for(var z = 0; z < rankings.brands.length; z++){
              		if(rankings.brands[z].brand_id ===  brandID){
              			for (var j = 0; j < rankings.brands[z].leaderboards.custom_leaderboards.length; j++){
              				if (rankings.brands[z].leaderboards.custom_leaderboards[j].id === $scope.rewards[i].leaderboard_ref){
              					$scope.rewards[i].rank = rankings.brands[z].leaderboards.custom_leaderboards[j].rank;
              					$scope.rewards[i].current_points = rankings.brands[z].leaderboards.custom_leaderboards[j].points;
              				}
              			}
              		}
              	}
              }
              //create array of rewards user has already claimed 
              if($scope.rewards[i].claims){
              	for(var claim in $scope.rewards[i].claims){
              		if ($scope.rewards[i].claims.hasOwnProperty(claim)){
              			if($scope.rewards[i].claims[claim].advocate_id === $scope.user_id){
              				$scope.userRewards[$scope.rewards[i].$id] = $scope.rewards[i];
              				break
              			};
              		}
              	};
              }

          };
          	$scope.rewardsLoading = false;
            $scope.brandLoading = false;
      },
      function(error){
      	alert('Oops! Something went wrong.');
      	$state.go('rewards');
      });
      },function(error){
      	alert('Oops! Something went wrong.');
      	$state.go('rewards');
      });
 	};

  //In case user comes to nested state from push notification
  if($stateParams && $stateParams.brandId){
  	loadRewards($stateParams.brandId);
  };

  $scope.toggleMessages = function() {
  	$scope.showMessages = !$scope.showMessages;
  }

  //Brief description of how reward is unlocked
  $scope.rewardString = function(reward) {
  	var rewardString = null;
  	switch(reward.type){
  		case 'CONTEST':
  		rewardString = 'Rewarded by contest';
  		break;
  		case 'POST':
  		rewardString = 'Rewarded for first post';
  		break;
  		case 'SIGNUP':
  		rewardString = 'Rewarded at signup';
  		break;
  		case 'POINTS':
  		rewardString = 'Rewarded at ' + reward.points_thresh + ' points';
  		break;
  		default:
  		rewardString = null;
  	}
  	return rewardString;
  }

  $scope.openRewardModal = function(reward, isUnlocked) {
  	$scope.selectedReward = reward;
  	$scope.selectedReward.isUnlocked = isUnlocked;
  	$scope.setRequirementString(reward);
  	ModalService.showModal({
  		templateUrl: "views/reward-modal.html",
  		controller: "RewardsCtrl",
  		scope: $scope
  	}).then(function(modal) {
  		modal.element.modal();
  	});
  }

  $scope.openInfoModal = function() {
  	ModalService.showModal({
  		templateUrl: "views/info-modal.html",
  		controller: "MainCtrl"
  	}).then(function(modal) {
  		modal.element.modal();
  	});
  }



//Detailed string of how reward is unlocked
$scope.setRequirementString = function(reward) {
	switch(reward.type){
		case 'CONTEST':
		if(reward.leaderboard_ref){
			Leaderboards.getLeaderboard($scope.selectedBrand.$id, reward.leaderboard_ref)
			.then(function(leaderboard){
				var start_date = $filter('date')(leaderboard.start, "MM/dd/yyyy");
				var end_date = $filter('date')(leaderboard.end, "MM/dd/yyyy");
				$scope.requirementString = 'Have a top ' + reward.top_n + ' score from ' + start_date + ' to ' + end_date + '.';
			});
		}
		else{
			$scope.requirementString = 'Error retrieving requirements!';
		};
		break;
		case 'POST':
		$scope.requirementString = 'Share a post for this brand to unlock this reward.';
		break;
		case 'SIGNUP':
		$scope.requirementString = 'This reward unlocks when you first sign up.';
		break;
		case 'POINTS':
		$scope.requirementString = 'Rewarded at ' + reward.points_thresh + ' points.';
		break;
		default:
		$scope.requirementString = 'Error retrieving requirements!';
	}

}

});
