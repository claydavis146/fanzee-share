'use strict';

/**
 * @ngdoc function
 * @name fanzeeWebApp.controller:LoginCtrl
 * @description
 * # Dashboard
 * Controller of the fanzeeShareApp
 */
 angular.module('fanzeeShareApp')
 .controller( 'LoginCtrl', function($scope, auth, $location, OpenData, $rootScope, $stateParams, $timeout, $state, store, ngToast) {

  $scope.loading = true;

  if(auth.isAuthenticated){
    console.log(auth.profile);
    $scope.loggedIn = true;
  }
  else {
    $scope.loggedIn = false;
  }

  /*Load brand data from various stateParams or rootScope variable, else no brand specified*/
  function loadBrand() {
    if($stateParams.brandId || $rootScope.redirectData && $rootScope.redirectData.brandId) {
      if ($stateParams.brandId){
        OpenData.brandInfo($stateParams.brandId)
        .then(function(info){
          $scope.loading = false;
          $scope.brand = info;
        },
        function(err){
          $scope.brand = null;
          $scope.loading = false;
          console.log(err);
        });
      }
      else{
        OpenData.brandInfo($rootScope.redirectData.brandId)
        .then(function(info){
          console.log('info ', info);
          $scope.loading = false;
          $scope.brand = info;
        },
        function(err){
          $scope.brand = null;
          $scope.loading = false;
          console.log(err);
        });
      }
    }
    else {
      $scope.brand = null;
      $scope.loading = false;
    }
  }

  $timeout(loadBrand());

  $scope.signin = function() {

    if($scope.brand){
      var logo = $scope.brand.logo;
    }
    else {
      var logo = 'https://fanzee.com/images/white-menu-logo.ea2497ed.png';
    }

    auth.signin({ disableSignupAction: false, connections: ['facebook', 'twitter', 'linkedin', 'instagram'], icon: logo, primaryColor: '#108FAD'},
     function(profile, token, accessToken, state, refreshToken) {
      store.set('profile', profile);
      store.set('token', token);
      store.set('refreshToken', refreshToken);
      auth.getToken({
        api: 'firebase'
      }).then(function(delegation) {
        store.set('firebaseToken', delegation.id_token);
        if($stateParams.brandId || $rootScope.redirectData && $rootScope.redirectData.brandId){
        var brandId = $stateParams.brandId || $rootScope.redirectData.brandId;
        $scope.loading = true;
        OpenData.addConnection(brandId).then(function(){
          $scope.loading = false;
          if($rootScope.redirectData && $rootScope.redirectData.redirectURL){
          var redirect = $rootScope.redirectData.redirectURL;
          $location.path(redirect);
        }
        $scope.loggedIn = true;
        });
      }
      else {
        $scope.loggedIn = true;
        $location.path('/');
      }
      }, function(error) {
        // Error getting the firebase token
        ngToast.danger({
          content: 'Please disable popup or ad blockers!'
        });
      })
    }, function() {
      // Error callback
      ngToast.danger({
        content: 'Please disable popup or ad blockers!'
      });
    });
  }
})

 .controller( 'BrandLoginCtrl', function($scope, auth, OpenData, Connections, $stateParams, $state, store, ngToast) {

  $scope.connectCheck = function(){
    var brandId = $stateParams.brandId;
    if(auth.isAuthenticated && brandId){
      OpenData.addConnection(brandId).then(function(){
        return
      },
      function(err){
        alert(err);
      })
    }
  }

});